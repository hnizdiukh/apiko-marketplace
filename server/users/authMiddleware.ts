import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';

require('dotenv').config();

const authMiddleware = async (req: Request, res: Response, next: any) => {
  try {
    const { headers: { authorization } } = req;

    if (!authorization) {
      res.status(401);
      throw new Error('Not authorized');
    }
    const token = authorization.split(' ')[1];

    if (!token) {
      res.status(401);
      throw new Error('Not authorized');
    }

    if ((await jwt.verify(token, process.env.JWT_KEY || '3cab0ec2386b73e5ca92c4799f9441cbf58820dbedbf8a99'))) {
      await next();
    } else {
      res.status(401);
      throw new Error('Invalid JWT token');
    }
  } catch (error) {
    next(error);
  }
};

export default authMiddleware;
