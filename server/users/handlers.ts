import bcrypt from 'bcrypt';
import { NextFunction, Request, Response } from 'express';
import { FindOneResult, ICollection } from 'monk';
import slugify from 'slugify';
import { userSchema, userUpdateSchema } from '../schemas';
import { Reset, User, UserUpdate } from './interfaces';
import DB from '../db';
import getUserFromJWT from '../utils/getUserFromJWT';
import validateAuthHeaders from '../utils/validateAuthHeaders';
import generateToken from '../utils/generateToken';
import nodemailer from 'nodemailer';
import { v4 as uuidv4 } from 'uuid';

require('dotenv').config();

const usersCollection: ICollection<User> = DB.get('users');
const resetCollection: ICollection<Reset> = DB.get('reset');

export const login = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { body } = req;

    const user: FindOneResult<User> = body.email ? await usersCollection.findOne({ email: body.email.toLowerCase() }) : null;

    if (!user) {
      throw new Error('Wrong credentials');
    }

    const passwordMatch = await bcrypt.compareSync(
      body.password,
      user.password || '',
    );

    if (!passwordMatch) {
      throw new Error('Wrong password');
    }

    const token = generateToken({
      iss: user.email,
    });

    if (!token) {
      throw new Error('JWT token is not created');
    }

    const { password, ...userResponse } = user;

    res.status(200);
    res.json({ token, user: userResponse });
  } catch (error) {
    res.status(401);
    next(error);
  }
};

export const register = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { body } = req;

    const user = (await userSchema.validate(body)) as User;
    
    const existedUser = await usersCollection.findOne({ email: user.email });

    if (existedUser) {
      throw new Error('User with such email is already exist');
    }

    const slug = slugify(user.fullName, { lower: true, strict: true, locale: 'en' });
    user.id = `${slug}-${(Date.now() % 1000) + 1}`;
    user.createdAt = Date.now();
    user.password = await bcrypt.hashSync(user.password, 10);

    const token = generateToken({
      iss: user.email,
    });

    if (!token) {
      throw new Error('JWT token is not created');
    }

    user.location = body.location || null;
    user.avatar = body.avatar || null;
    user.phone = body.phone || null;
    user.updatedAt = body.updatedAt || null;
    user.email = user.email.toLowerCase();

    await usersCollection.insert(user);

    const { password, ...userResponse } = user;

    res.status(201);
    res.json({ token, user: userResponse });
  } catch (error) {
    res.status(401);
    next(error);
  }
};

export const getCurrentUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token);

    if (!user) {
      throw new Error('Wrong JWT token');
    }

    const { password, ...userResponse } = user;

    res.status(200);
    res.json(userResponse);
  } catch (error) {
    res.status(401);
    next(error);
  }
};

export const getUser = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  try {
    const user = await usersCollection.findOne({ id });

    if (!user) {
      throw new Error('No user with such id');
    }

    const { password, ...userResponse } = user;
    res.json(userResponse);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const updateUser = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { body } = req;

    const fieldsToUpdate: UserUpdate = await userUpdateSchema.validate(
      body, { stripUnknown: true }
    ) as UserUpdate;

    const token = await validateAuthHeaders(req);
    const currentUser = await getUserFromJWT(token);

    if (!currentUser) {
      throw new Error('No user with such id');
    }

    fieldsToUpdate.updatedAt = Date.now();

    const updatedUserState: any = await usersCollection.update(
      { id: currentUser.id },
      { $set: { ...fieldsToUpdate } },
    );

    if (!updatedUserState.nModified && updatedUserState.ok) {
      throw new Error('No fields to update');
    }

    if (!updatedUserState.nModified && !updatedUserState.ok) {
      throw new Error('Can not be updated');
    }

    const updatedUser = await usersCollection.findOne({ id: currentUser.id });

    if (!updatedUser) {
      throw new Error('Something wrong with user id');
    }

    const { password, ...response } = updatedUser;
    res.json(response);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const forgot = async (req: Request, res: Response, next: NextFunction) => {
  const { email } = req.body;
  try {

    if (!email) {
      throw new Error('Email is required');
    }

    const user = await usersCollection.findOne({ email: email.toLowerCase() });

    if (!user) {
      throw new Error('No user with such email');
    }

    const transporter = nodemailer.createTransport({host: process.env.EMAIL_HOST,
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_LOGIN,
      pass: process.env.EMAIL_PASSWORD
    }})

    const id = uuidv4();

    const info = await transporter.sendMail({
      from: process.env.EMAIL_LOGIN,
      to: email,
      subject: "Marketplace Restore password",
      text: "Password reset link",
      html: `<p>Click on this link to restore your password for Marketplace website</p>
      ${req.protocol}://${req.headers.host}/#/reset/${id}/
      `,
    });

    await resetCollection.remove({ email });
    await resetCollection.insert({ id, email, createdAt: Date.now() });

    const { password, ...userResponse } = user;
    res.json(userResponse);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const reset = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const { newPassword } = req.body;

    if (!newPassword) {
      throw new Error("newPassword is required");
    }

    const hashedPassword = await bcrypt.hashSync(newPassword, 10);

    const reset: FindOneResult<Reset> = await resetCollection.findOne({ id });

    if(!reset || !reset.email) {
      throw new Error("Link for reset is not valid");
    }

    await usersCollection.update(
      { email: reset.email },
      { $set: { password: hashedPassword } },
    );

    await resetCollection.remove({ email: reset.email });

    res.status(200);
    res.json('OK');
  } catch (error) {
    res.status(422);
    next(error);
  }
}

export const checkReset =  async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;

    const reset: FindOneResult<Reset> = await resetCollection.findOne({ id });

    res.status(200);
    res.json(reset);
  } catch (error) {
    res.status(422);
    next(error);
  }
}
