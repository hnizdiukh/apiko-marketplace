export interface User {
  id: string;
  _id: object;
  fullName: string;
  email: string;
  password?: string | null;
  createdAt: number;
  updatedAt?: number | null;
  location?: string | null;
  avatar?: string | null;
  phone?: string | null;
}

export interface UserUpdate {
  fullName?: string,
  avatar?: string,
  phone?: string,
  location?: string,
  updatedAt?: number | null
}

export interface Reset {
  id: string;
  _id?: object;
  email: string;
  createdAt: number;
}
