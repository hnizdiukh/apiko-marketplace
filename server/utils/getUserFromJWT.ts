import jwt from 'jsonwebtoken';
import DB from '../db';

require('dotenv').config();

const usersCollection = DB.get('users');

const getUserFromJWT = async (token: any) => {
  try{
    if (!token) {
      return;
    }

    const decoded: any = await jwt.verify(token, process.env.JWT_KEY || '');

    if (!decoded || !decoded.iss) {
      throw new Error('Invalid JWT');
    }

    const user = await usersCollection.findOne({ email: decoded.iss });

    if (!user) {
      return;
    }

    delete user.password;

    return user;
  } catch (error) {
    return;
  }
};

export default getUserFromJWT;
