import { Request } from 'express';

const validateAuthHeaders = (req: Request) => {
  const { headers: { authorization } } = req;

  if (!authorization) {
    return;
  }
  const token = authorization.split(' ')[1];
  return token;
};

export default validateAuthHeaders;
