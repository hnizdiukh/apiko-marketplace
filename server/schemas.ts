import * as yup from 'yup';

export const productSchema = yup.object({
  ownerId: yup.string(),
  title: yup.string().trim().min(2).required(),
  description: yup.string(),
  photos: yup.array(yup.string().trim().url()),
  location: yup.string().trim().min(2).required(),
  price: yup.number().required(),
  createdAt: yup.number().nullable(),
  updatedAt: yup.number().nullable(),
  saved: yup.boolean(),
});

export const userSchema = yup.object({
  email: yup.string().email().trim().min(4)
  .required(),
  password: yup.string().trim().min(4).required(),
  fullName: yup.string().trim().min(2).required(),
  createdAt: yup.number().nullable(),
  updatedAt: yup.number().nullable(),
  location: yup.string().trim().min(1),
  avatar: yup.string().trim().min(3).url(),
  phone: yup.string().nullable(),
});

export const userUpdateSchema = yup.object({
  fullName: yup.string().trim().min(2),
  location: yup.string().trim().min(1),
  avatar: yup.string().trim().url().nullable(),
  phone: yup.string().trim().min(5).nullable(),
  updatedAt: yup.number().nullable(),
});
