export interface Product {
  _id?: object;
  id: string;
  ownerId: string;
  title: string;
  description?: string;
  photos: string[];
  location: string;
  price: number;
  createdAt?: number | null;
  updatedAt?: number | null;
  saved: boolean;
  owner: object;
}

export interface SearchFields {
  keywords?: string;
  location?: string;
  priceFrom?: number;
  priceTo?: number;
  limit?: number;
  offset?: number;
}

export interface SavedProducts {
  userId: string;
  productIds: Array<string> | string;
  $addToSet?: object;
  $pull?: object;
}
