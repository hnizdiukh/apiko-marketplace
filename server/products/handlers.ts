/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
import { NextFunction, Request, Response } from 'express';
import { ICollection } from 'monk';
import slugify from 'slugify';
import { productSchema } from '../schemas';
import { Product, SearchFields, SavedProducts } from './interfaces';
import DB from '../db';
import getUserFromJWT from '../utils/getUserFromJWT';
import validateAuthHeaders from '../utils/validateAuthHeaders';
import { User } from '../users/interfaces';

const productCollection: ICollection<Product> = DB.get('products');
const savedProductCollection: ICollection<SavedProducts> = DB.get('saved_products');
const usersCollection: ICollection<User> = DB.get('users');

export const getProducts = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { body } = req;
    const { params } = req;

    const limit: number = (body && body.value) ? body.value.limit : (params.limit || 20);
    const offset: number = (body && body.value) ? body.value.offset : (params.offset || 0);

    const products: Array<Product> = await productCollection.aggregate([
      { $sort: { createdAt: -1 } },
      { $skip: Number(offset) },
      { $limit: Number(limit) },
    ]);

    const token = await validateAuthHeaders(req);

    const currentUser = token ? await getUserFromJWT(token) : null;

    const runAsyncFunctions = async (user: User) => {
      if (!user) {
        products.map((product) => (product.saved = false));
        return;
      }

      let saved: any;
      await Promise.all(
        products.map(async (product) => {
          saved = await savedProductCollection.find({ userId: user.id, productIds: product.id });
          product.saved = !!saved.length;
        })
      );
    };

    await runAsyncFunctions(currentUser);

    if (!products) {
      throw new Error('No products found');
    }

    res.status(200);
    res.send(products);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const postProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { body } = req;

    const product: Product = (await productSchema.validate(
      body,
      { stripUnknown: true }
    )) as Product;
    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token);
    const slug = slugify(product.title, { lower: true, strict: true, locale: 'en' });
    product.id = `${slug}-${(Date.now() % 1000) + 1}`;
    product.createdAt = Date.now();
    product.ownerId = user.id;

    await productCollection.insert(product);

    res.json(product);
    res.status(201);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const getSingleProduct = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  try {
    const product = await productCollection.findOne({ id });

    if (!product) {
      throw new Error(`No product with such id ${id}`);
    }

    const owner: User = await usersCollection.findOne({ id: product.ownerId }) as User;

    const { password, ...ownerResponse } = owner;

    if (!owner) {
      throw new Error('Product owner not found');
    }

    product.owner = ownerResponse;

    res.json(product);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const updateProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const product = await productCollection.findOne({ id });

    if (!product) {
      throw new Error(`No product with such id ${id}`);
    }

    const productToUpdate: Product = (await productSchema.validate(
      body,
      { stripUnknown: true }
    )) as Product;

    productToUpdate.updatedAt = Date.now();

    await productCollection.update({ id }, { $set: { ...productToUpdate } });

    res.json(productToUpdate);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const savedProducts = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token);
    const userSaved: SavedProducts = await savedProductCollection.findOne(
      { userId: user.id }
    ) as SavedProducts;

    if (userSaved && userSaved.productIds) {
      const products: Array<Product> = await productCollection.aggregate([
        { $sort: { createdAt: -1 } },
        { $match: { id: { $in: userSaved.productIds } } },
      ]);

      await products.map((product: Product) => (product.saved = true));

      res.json(products);
    } else {
      res.json([]);
    }

    res.status(200);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const searchProducts = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const params = req.query;
    if (Object.keys(params).length === 0) {
      throw new Error('Parameters are not specified');
    }
    const { keywords, location, priceFrom, priceTo, limit, offset }: SearchFields = params;

    let searchQuery: any = '';
    let sortByScore: any = '';

    if (keywords && keywords.length) {
      searchQuery = {
        $search: {
          text: {
            query: keywords.replace('+', ' ').toString(),
            path: ['title', 'description'],
          },
        },
      };

      sortByScore = {
        $sort: {
          score: {
            $meta: 'textScore',
          },
        },
      };
    }

    const queryMatch: any = { $match: {} };

    if (priceTo) {
      queryMatch.$match.price = { $gte: Number(priceFrom) || 0, $lte: Number(priceTo) };
    } else {
      queryMatch.$match.price = { $gte: Number(priceFrom) || 0 };
    }

    if (location) {
      queryMatch.$match.location = location.replace('+', ' ');
    }

    const products: Array<Product> = await productCollection.aggregate(searchQuery ? [
      searchQuery,
      queryMatch,
      {
        $skip: offset || 0,
      },
      {
        $limit: limit || 20,
      },

      sortByScore,
    ] : [queryMatch,
        {
          $skip: offset || 0,
        },
        {
          $limit: limit || 20,
        }]);

    res.json(products);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const userProducts = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;

    if (!id) {
      throw new Error('id should be specified');
    }

    const products = await productCollection.aggregate([
      { $sort: { createdAt: -1 } },
      { $match: { ownerId: id } },
    ]);

    res.json({ list: products, count: products.length });
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const saveProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { params } = req;

    const productId: string = params.id as string;

    if (!productId) {
      throw new Error('productId is not specified');
    }

    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token);

    const usersSaved: SavedProducts = await savedProductCollection.findOne(
      { userId: user.id }
    ) as SavedProducts;
    const product: Product = await productCollection.findOne({ id: productId }) as Product;
    let updated: any; let
created;

    if (!productId || !product) {
      throw new Error(`No product with such id ${productId}`);
    }

    if (usersSaved) {
      updated = await savedProductCollection.update(
        { userId: user.id },
        { $addToSet: { productIds: productId } }
      );
    } else {
      created = await savedProductCollection.insert({
        userId: user.id,
        productIds: [productId],
      });
    }

    res.json((updated && updated.nModified) || created ? 'Saved' : 'Already was saved');
    res.status(200);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const unsaveProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { params } = req;

    const productId: string = params.id;

    if (!productId) {
      throw new Error('productId is not specified');
    }

    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token);
    const updated: any = await savedProductCollection.update(
      { userId: user.id },
      { $pull: { productIds: productId } }
    );

    res.json(updated && updated.nModified ? 'Unsaved' : 'Not in saved');
    res.status(200);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const removeProduct = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const product: Product = await productCollection.findOne({ id }) as Product;

    const token = await validateAuthHeaders(req);
    const user = await getUserFromJWT(token);

    if (!product) {
      throw new Error(`No product with such id ${id}`);
    }

    if (user.id !== product.ownerId) {
      throw new Error('User is not product owner');
    }

    const removed = await productCollection.remove({ id });

    res.json(removed);
  } catch (error) {
    res.status(422);
    next(error);
  }
};
