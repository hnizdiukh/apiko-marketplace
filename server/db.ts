import monk from 'monk';

require('dotenv').config();

const DB = monk(process.env.MONGODB_URI || '');

export default DB;
