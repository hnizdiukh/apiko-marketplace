import { ICollection } from 'monk';
import { NextFunction, Request, Response } from 'express';
import DB from '../db';
import getUserFromJWT from '../utils/getUserFromJWT';
import validateAuthHeaders from '../utils/validateAuthHeaders';
import { Chat, Message } from './interfaces';
import { insertMessage } from './db';
import { Product } from '../products/interfaces';
import { User } from '../users/interfaces';

const productCollection: ICollection<Product> = DB.get('products');
const chatCollection: ICollection<Chat> = DB.get('chats');
const messageCollection: ICollection<Message> = DB.get('messages');
const usersCollection: ICollection<User> = DB.get('users');

export const createChat = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { params } = req;
    const productId = params.id;


    if (!productId) {
      throw new Error('id is not specified');
    }

    const product: Product = await productCollection.findOne({ id: productId }) as Product;
    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token) as User;

    if (!product) {
      throw new Error(`No product with such id ${productId}`);
    }

    const owner: User = await usersCollection.findOne({ id: product.ownerId }) as User;

    delete owner.password;

    if (!owner) {
      throw new Error('Owner of the product is not found');
    }

    if (user.id === owner.id) {
      throw new Error('Can not create chat with yourself');
    }

    const existingChat = await chatCollection.findOne({
      product,
      participants: [owner, user],
    });

    if (existingChat) {
      res.json(existingChat);
      return;
    }

    const chat: Chat = {
      id: `${product.id}-${user.id}`,
      productId,
      ownerId: product.ownerId,
      createdAt: Date.now(),
      updatedAt: Date.now(),
      product: { ...product },
      participants: [owner, user],
      messagesCount: 0,
    };

    await chatCollection.insert({ ...chat });
    res.json(chat);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const getChats = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = await validateAuthHeaders(req);
    const user: User = await getUserFromJWT(token);

    if (!user) {
      throw new Error('Not authorized');
    }

    const chats = await chatCollection.aggregate([
      { $match: { participants: { $elemMatch: { id: user.id } } } },
      { $sort: { updatedAt: -1 } },
    ]);

    res.json(chats);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const sendMessage = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { params } = req;
    const { body } = req;
    
    if (!body.text) {
      throw new Error('"text" is required');
    }
    const chatId: string = params.id;
    
    if (!chatId) {
      throw new Error('Invalid chat id');
    }
    
    const token = await validateAuthHeaders(req);
    const user = await getUserFromJWT(token);

    const message = await insertMessage({ chatId, userId: user.id, text: body.text });

    await chatCollection.update({ id: chatId }, { $set: { lastMessage: message, updatedAt: Date.now() }, $inc: { messagesCount: 1 } });

    res.json(message);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

export const getMessages = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { params } = req;
    const { query } = req;

    const limit: number = Number(query.limit) || 20;
    const offset: number = Number(query.offset) || 0;
    const chatId: string | undefined = params.id;

    if (!chatId) {
      throw new Error('Invalid id');
    }

    const chat = await chatCollection.findOne({ id: chatId });
    const token = await validateAuthHeaders(req);
    const user = await getUserFromJWT(token);

    if (!user) {
      throw new Error('Invalid JWT');
    }

    if (chat ? !chat.participants.some((u) => u.id === user.id) : true) {
      throw new Error('User is not a participant for this chat');
    }

    const messages: Array<Message> = await messageCollection.aggregate([
      { $match: { chatId } },
      { $sort: { createdAt: -1 } },
      { $skip: offset },
      { $limit: limit },
    ]);
    
    const count = await messageCollection.count({ chatId });

    if (!messages) {
      throw new Error('No messages found');
    }

    res.json({ left: Math.max(0, count - (offset + limit)), list: messages });
    res.status(200);
  } catch (error) {
    res.status(422);
    next(error);
  }
};
