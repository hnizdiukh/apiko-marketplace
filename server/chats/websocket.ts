import { Application, Request, RequestHandler } from 'express';
import WebSocket, { ServerOptions, Server } from 'ws';
import getUserFromJWT from '../utils/getUserFromJWT';
import { getChatPartner } from './db';
import url from 'url';

require('dotenv').config();

const webSockets: any = new Map();

const startWebSocket = (app: any) => {
  try {

    const wss = new Server({ server: app });

    console.log('Starting websocket');
  
    wss.on('connection', (ws: WebSocket) => {
      ws.on('message', async (message: any) => {
        const parsed = JSON.parse(message);

        let user; let partner; let partnerWS;

        switch (parsed.type) {
          case 'connection':
            if (!parsed.data) {
              break;
            }
            user = await getUserFromJWT(parsed.data.toString());

            if (!user) {
              break;
            }

            webSockets.set(user.id, ws);
            break;
          case 'message':
            partner = await getChatPartner(parsed.data.chatId, parsed.data.ownerId);
            partnerWS = webSockets.get(partner.id);
            if (partnerWS && partnerWS.readyState === 1) {
              partnerWS.send(JSON.stringify(parsed.data));
            }
            break;
          default:
            break;
        }
      });

      ws.on('close', (e: any) => {
        console.log('WS user disconected ', e);
      });
    });
  } catch (error) {
    console.error(error);
  }
};

export default startWebSocket;
