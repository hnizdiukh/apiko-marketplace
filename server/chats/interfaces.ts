import { Product } from '../products/interfaces';
import { User } from '../users/interfaces';

export interface Message {
  id: number,
  chatId: string,
  ownerId: string,
  text: string,
  read: boolean,
  createdAt: number,
  updatedAt?: number | null
}

export interface Chat {
  _id?: object;
  id: string,
  productId: string,
  ownerId: string,
  createdAt?: number,
  updatedAt?: number,
  lastMessage?: Message,
  product: Product,
  participants: [User, User],
  messagesCount: number,
}
