import { ICollection } from 'monk';
import DB from '../db';
import { Chat, Message } from './interfaces';

const messageCollection: ICollection<Message> = DB.get('messages');
const chatCollection: ICollection<Chat> = DB.get('chats');

export const insertMessage = async (msg: any) => {
  const { chatId, userId, text } = msg;
  const messagesCount = await messageCollection.count({ chatId });

  const message: Message = {
    id: messagesCount,
    chatId,
    ownerId: userId,
    text,
    read: false,
    createdAt: Date.now(),
    updatedAt: null
  };

  await messageCollection.insert(message);

  return message;
};

export const getChatPartner = async (chatId: string, userId: string) => {
  const chat = await chatCollection.findOne({ id: chatId });
  const partner = chat ? chat.participants.filter((p: any) => p.id !== userId) : [];
  return partner[0];
};
