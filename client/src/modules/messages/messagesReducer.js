import { handleActions } from '@letapp/redux-actions';
import * as actions from './messagesActions';

const INITIAL_STATE = {
  fetchMessages: {
    isLoading: false,
    isError: false,
    error: null,
  },
  messages: new Map(),
  messagesTriggerCount: 0,
  messagesLeft: null,
  lastMessage: null,
  sendMessage: {
    isLoading: false,
    isError: false,
    error: null,
  },
};

export default handleActions(
  {
    // LOAD MESSAGES

    [actions.fetchMessages.start]: (state) => ({
      ...state,
      fetchMessages: {
        ...state.fetchMessages,
        isLoading: true,
        error: null,
        isError: false,
      },
    }),
    [actions.fetchMessages.success]: (state, { payload: { chatId, data: { list, left } } }) => ({
      ...state,
      fetchMessages: {
        ...state.fetchMessages,
        isLoading: false,
      },
      messages: state.messages.has(chatId)
        ? state.messages.set(chatId,
            [...list.reverse(), ...state.messages.get(chatId)])
        : state.messages.set(chatId, list.reverse()),
      messagesTriggerCount: state.messagesTriggerCount + 1,
      messagesLeft: left,
    }),
    [actions.fetchMessages.error]: (state, action) => ({
      ...state,
      fetchMessages: {
        ...state.fetchMessages,
        isLoading: false,
        error: action.payload,
        isError: true,
      },
    }),

    // SEND MESSAGE

    [actions.sendMessage.start]: (state, action) => ({
      ...state,
      sendMessage: {
        ...state.sendMessage,
        isLoading: true,
        error: null,
        isError: false,
      },
      messages: state.messages.has(action.payload.chatId)
        ? state.messages.set(action.payload.chatId,
          [...state.messages.get(action.payload.chatId), action.payload])
        : state.messages.set(action.payload.chatId, [action.payload]),
      lastMessage: action.payload,
      messagesTriggerCount: state.messagesTriggerCount + 1
    }),
    [actions.sendMessage.success]: (state) => ({
      ...state,
      sendMessage: {
        ...state.sendMessage,
        isLoading: false,
      },
      messages: state.messages
    }),
    [actions.sendMessage.error]: (state, action) => ({
      ...state,
      sendMessage: {
        ...state.sendMessage,
        isLoading: false,
        error: action.payload,
        isError: true,
      },
    }),
  },
  INITIAL_STATE
);
