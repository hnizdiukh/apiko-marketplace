import reducer from './messagesReducer';
import * as messagesActions from './messagesActions';
import * as messagesOperations from './messagesOperations';

export { messagesActions, messagesOperations };

export default reducer;
