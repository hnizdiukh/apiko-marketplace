export const getUser = (state) => state.viewer.user;
export const getIsViewerLoading = (state) => state.viewer.fetchViewer.isLoading;
