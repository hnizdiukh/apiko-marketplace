import createAsyncActions from '@letapp/redux-actions/lib/createAsyncActions';

// eslint-disable-next-line import/prefer-default-export
export const fetchUser = createAsyncActions('user/FETCH');
