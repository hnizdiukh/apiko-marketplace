import { handleActions } from '@letapp/redux-actions';
import * as actions from './userActions';

const INITIAL_STATE = {
  fetchUser: {
    isLoading: false,
    isError: false,
    error: null
  },
  user: null
};

export default handleActions(
  {
    [actions.fetchUser.start]: (state) => ({
      ...state,
      fetchUser: {
        ...state.fetchUser,
        isLoading: true,
        error: null,
        isError: false
      }
    }),
    [actions.fetchUser.success]: (state, action) => ({
      ...state,
      fetchUser: {
        ...state.fetchUser,
        isLoading: false
      },
      user: action.payload
    }),
    [actions.fetchUser.error]: (state, action) => ({
      ...state,
      fetchUser: {
        ...state.fetchUser,
        isLoading: false,
        error: action.payload,
        isError: true
      }
    })
  },
  INITIAL_STATE
);
