import Api from 'src/api';
import * as actions from './userActions';

// eslint-disable-next-line import/prefer-default-export
export function fetchUser(userId) {
  return async function fetchUserThunk(dispatch) {
    try {
      dispatch(actions.fetchUser.start());

      const res = await Api.User.get(userId);

      dispatch(actions.fetchUser.success(res.data));
    } catch (err) {
      dispatch(
        actions.fetchUser.error({
          message: err.message,
        })
      );
    }
  };
}
