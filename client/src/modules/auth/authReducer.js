import { handleActions } from '@letapp/redux-actions';
import * as actions from './authActions';

const INITIAL_STATE = {
  login: {
    isLoading: false,
    isError: false,
    error: null,
    isSuccess: false,
  },
  register: {
    isLoading: false,
    isError: false,
    error: null,
    isSuccess: false,
  },
  logout: {
    isLoading: false,
    isError: false,
    error: null,
  },
  forgot: {
    isLoading: false,
    isError: false,
    error: null,
    isSuccess: false,
  },
  reset: {
    isLoading: false,
    isError: false,
    error: null,
    isSuccess: false,
  },
  checkReset: {
    isLoading: false,
    isError: false,
    error: null,
    isSuccess: false,
  },
};

export default handleActions(
  {
    // LOGIN

    [actions.login.start]: (state) => ({
      ...state,
      login: {
        ...state.login,
        isLoading: true,
        error: null,
        isError: false,
        isSuccess: false,
      },
    }),
    [actions.login.success]: (state) => ({
      ...state,
      login: {
        ...state.login,
        isLoading: false,
        isSuccess: true,
      },
    }),
    [actions.login.error]: (state, action) => ({
      ...state,
      login: {
        ...state.login,
        isLoading: false,
        error: action.payload,
        isError: true,
        isSuccess: false,
      },
    }),

    // REGISTER

    [actions.register.start]: (state) => ({
      ...state,
      register: {
        ...state.register,
        isLoading: true,
        error: null,
        isError: false,
        isSuccess: false,
      },
    }),
    [actions.register.success]: (state) => ({
      ...state,
      register: {
        ...state.register,
        isLoading: false,
        isSuccess: true,
      },
    }),
    [actions.register.error]: (state, action) => ({
      ...state,
      register: {
        ...state.register,
        isLoading: false,
        error: action.payload,
        isError: true,
        isSuccess: false,
      },
    }),

    // LOGOUT

    [actions.logout.start]: (state) => ({
      ...state,
      logout: {
        ...state.logout,
        isLoading: true,
        error: null,
        isError: false,
      },
    }),
    [actions.logout.success]: (state) => ({
      ...state,
      logout: {
        ...state.logout,
        isLoading: false,
      },
    }),
    [actions.logout.error]: (state, action) => ({
      ...state,
      logout: {
        ...state.logout,
        isLoading: false,
        error: action.payload,
        isError: true,
      },
    }),

    // FORGOT

    [actions.forgot.start]: (state) => ({
      ...state,
      forgot: {
        ...state.forgot,
        isLoading: true,
        error: null,
        isError: false,
        isSuccess: false,
      },
    }),
    [actions.forgot.success]: (state) => ({
      ...state,
      forgot: {
        ...state.forgot,
        isLoading: false,
        isSuccess: true,
      },
    }),
    [actions.forgot.error]: (state, action) => ({
      ...state,
      forgot: {
        ...state.forgot,
        isLoading: false,
        error: action.payload,
        isError: true,
        isSuccess: false,
      },
    }),

    // FORGOT

    [actions.reset.start]: (state) => ({
      ...state,
      reset: {
        ...state.reset,
        isLoading: true,
        error: null,
        isError: false,
        isSuccess: false,
      },
    }),
    [actions.reset.success]: (state) => ({
      ...state,
      reset: {
        ...state.reset,
        isLoading: false,
        isSuccess: true,
      },
    }),
    [actions.reset.error]: (state, action) => ({
      ...state,
      reset: {
        ...state.reset,
        isLoading: false,
        error: action.payload,
        isError: true,
        isSuccess: false,
      },
    }),

    // CHECK RESET

    [actions.checkReset.start]: (state) => ({
      ...state,
      checkReset: {
        ...state.checkReset,
        isLoading: true,
        error: null,
        isError: false,
        isSuccess: false,
      },
    }),
    [actions.checkReset.success]: (state) => ({
      ...state,
      checkReset: {
        ...state.checkReset,
        isLoading: false,
        isSuccess: true,
      },
    }),
    [actions.checkReset.error]: (state, action) => ({
      ...state,
      checkReset: {
        ...state.checkReset,
        isLoading: false,
        error: action.payload,
        isError: true,
        isSuccess: false,
      },
    }),
  },
  INITIAL_STATE
);
