import createAsyncActions from '@letapp/redux-actions/lib/createAsyncActions';

export const login = createAsyncActions('auth/LOGIN');
export const register = createAsyncActions('auth/REGISTER');
export const logout = createAsyncActions('auth/LOGOUT');
export const forgot = createAsyncActions('auth/FORGOT');
export const reset = createAsyncActions('auth/RESET');
export const checkReset = createAsyncActions('auth/CHECK_RESET');
