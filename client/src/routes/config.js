const routes = {
  HOME: '/',
  LOGIN: '/login',
  REGISTER: '/register',
  FORGOT: '/forgot',
  RESET: '/reset',
  RESET_ID: '/reset/:id',
  SELL: '/sell',
  WISH_LIST: '/wish_list',
  PROFILE: '/profile',
  USER: '/user',
  USER_ID: '/user/:userId',
  EDIT_PROFILE: '/edit_profile',
  PRODUCTS: '/products',
  PRODUCT: '/product',
  EDIT_PRODUCT_ID: '/product/:productId/edit',
  PRODUCT_ID: '/product/:productId',
  CHAT_MODAL: '/chats',
  CHAT: '/chats',
  CHAT_ID: '/chats/:chatId',

  PRIVACY: '/privacy',
};

export default routes;
