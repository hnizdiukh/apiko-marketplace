import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, useDispatch } from 'react-redux';

import { ToastContainer } from 'react-toastify';
import { HashRouter } from 'react-router-dom/cjs/react-router-dom.min';
import NavBar from './components/NavBar/NavBarContainer';

import store from './store/createStore';
import { appOperations } from './modules/app';

import Footer from './components/Footer/Footer';
import Routes from './routes/routes';

import './styles/App.css';
import './styles/buttons.css';
import './styles/mobile.css';
import './styles/tablet.css';
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  const dispatch = useDispatch();
  dispatch(appOperations.init());

  return (
    <HashRouter>
      <NavBar />
      <ToastContainer />
      <Routes />
      <Footer />
    </HashRouter>
  );
};

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#root')
);
