import * as Yup from 'yup';

export const ProductSchema = Yup.object({
  title: Yup.string().trim().min(2).required('Required'),
  location: Yup.string().min(4, 'Too Short!').max(50, 'Too Long!').required('Required'),
  description: Yup.string(),
  price: Yup.number().required('Required'),
  ownerId: Yup.string(),
  photos: Yup.array(Yup.string().trim().url()),
  createdAt: Yup.number().nullable(),
  updatedAt: Yup.number().nullable(),
  saved: Yup.boolean(),
});

export const ChatModalSchema = Yup.object().shape({
  text: Yup.string().required('Required'),
});

export const ProfileSchema = Yup.object().shape({
  fullName: Yup.string().min(4, 'Too Short!').max(50, 'Too Long!').required('Required'),
  phone: Yup.string().min(8, 'Invalid Number!').max(13, 'Invalid Number!'),
  location: Yup.string(),
  createdAt: Yup.number().nullable(),
  updatedAt: Yup.number().nullable(),
  avatar: Yup.string().trim().min(3).url(),
});

export const LoginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Required'),
  password: Yup.string().min(8, 'Too Short!').required('Required'),
});

export const SignupSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Required'),
  fullName: Yup.string().min(4, 'Too Short!').max(50, 'Too Long!').required('Required'),
  password: Yup.string().min(8, 'Too Short!').required('Required'),
  passwordConfirmation: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Required'),
});
