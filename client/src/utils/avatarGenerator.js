const generateAvatar = (fullName, id, saturation, lightness) => {
  const words = fullName.match(/[A-Z]*[A-Z]/g);

  const badge = words ? words.join('').substring(0, 2) : fullName.charAt(0).toUpperCase();

  let hash = 0;
  for (let i = 0; i < id.length; i += 1) {
    hash = id.charCodeAt(i) + (hash * 5 - hash);
  }

  const h = hash % 360;

  return { badge, color: `hsl(${h},${saturation}%,${lightness}%)` };
};

export default generateAvatar;
