import { productImagePlaceholder } from 'src/configs/variables';

const getProductImage = (photos) => {
  if (photos) {
    return photos[0] || productImagePlaceholder;
  }
  return productImagePlaceholder;
};

export default getProductImage;
