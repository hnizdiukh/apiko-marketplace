import axios from 'axios';
import SocketApi from './SocketApi';

const host = process.env.NODE_ENV === 'production' ? window.location.origin : 'http://localhost:8000';

const urlStart = `${host}/api/v1`;

const urls = {
  login: `${urlStart}/auth/login`,
  register: `${urlStart}/auth/register`,
  forgot: `${urlStart}/auth/forgot`,
  reset: `${urlStart}/auth/reset`,
  user: `${urlStart}/users`,
  getViewer: `${urlStart}/account/user`,
  putUser: `${urlStart}/account/user`,

  getProducts: `${urlStart}/products/latest`,
  getProduct: `${urlStart}/products`,
  removeProduct: `${urlStart}/products`,
  addProduct: `${urlStart}/products`,
  getSaved: `${urlStart}/products/saved`,

  imageUpload: `${urlStart}/upload/images`,

  search: `${urlStart}/products/search`,

  createChat: `${urlStart}/products`,
  chats: `${urlStart}/chats`,
  sendMessage: `${urlStart}/chats`,
  getMessages: `${urlStart}/chats`,

  ping: `${urlStart}/ping`
};

export const Auth = {
  _token: null,

  get isLoggedIn() {
    return !!this._token;
  },

  setToken(token) {
    this._token = token;

    this._storeToken(token);

    this._setTokenToAxios(token);
  },

  init() {
    axios.get(urls.ping);
    try {
      const token = window.localStorage.getItem('token');
      this._token = JSON.parse(token);

      this._setTokenToAxios(this._token);

      SocketApi.init(this._token);
    } catch (err) {
      console.error(err);
    }
  },

  login({ email, password }) {
    return axios.post(urls.login, { email, password });
  },

  logout() {
    this._token = null;
    try {
      window.localStorage.removeItem('token');
    } catch (err) {
      console.error(err);
    }
  },

  forgot(email) {
    return axios.post(urls.forgot, { email });
  },

  reset({ id, password: newPassword }) {
    return axios.post(`${urls.reset}/${id}`, { newPassword });
  },

  checkReset(id) {
    return axios.get(`${urls.reset}/${id}`);
  },

  register({ email, password, fullName }) {
    return axios.post(urls.register, { email, password, fullName });
  },

  _storeToken() {
    try {
      window.localStorage.setItem('token', JSON.stringify(this._token));
    } catch (error) {
      console.error(error);
    }
  },

  _setTokenToAxios(token) {
    if (token) axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  },
};

export const User = {
  get(userId) {
    return axios.get(`${urls.user}/${userId}`);
  },
};

export const Viewer = {
  get() {
    return axios.get(urls.getViewer);
  },
  put({ fullName, avatar, phone, location }) {
    return axios.put(urls.putUser, { fullName, avatar, phone, location });
  },
};

export const Products = {
  get({ offset = 0, limit = 20 }) {
    return axios.get(urls.getProducts, { params: { offset, limit } });
  },
  getProduct(productId) {
    return axios.get(`${urls.getProduct}/${productId}`);
  },
  add(product) {
    return axios.post(urls.addProduct, product);
  },
  update(product) {
    return axios.put(`${urlStart}/products/${product.id}`, product);
  },
  save(productId) {
    return axios.post(`${urlStart}/products/${productId}/save`);
  },
  unsave(productId) {
    return axios.post(`${urlStart}/products/${productId}/unsave`);
  },
  getSaved() {
    return axios.get(urls.getSaved);
  },
  search(query) {
    return axios.get(urls.search, { params: query });
  },
  userProducts(userId) {
    return axios.get(`${urlStart}/users/${userId}/products`);
  },
  remove(productId) {
    return axios.delete(`${urls.removeProduct}/${productId}`);
  },
};

export const Image = {
  upload(image) {
    const headers = {
      Authorization: 'Client-ID 86cb3b6828309ca',
      'Content-Type': 'multipart/form-data',
    };

    return axios({
      url: 'https://api.imgur.com/3/image',
      method: 'post',
      headers,
      data: image,
    });
  },
};

export const Chats = {
  createChat(productId) {
    return axios.post(`${urls.createChat}/${productId}/createChat`);
  },
  get() {
    return axios.get(urls.chats);
  },
};

export const Messages = {
  sendMessage(text, chatId) {
    return axios.post(`${urls.sendMessage}/${chatId}/messages`, text);
  },
  getMessages({ chatId, offset, limit }) {
    return axios.get(`${urls.getMessages}/${chatId}/messages`, { params: { offset, limit } });
  },
};

export function init() {
  Auth.init();
}
