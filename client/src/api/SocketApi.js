import { w3cwebsocket as W3CWebSocket } from 'websocket';

const host = process.env.NODE_ENV === 'production' ? window.location.origin.replace(/^http/, 'ws') : 'ws://localhost:8000';

class SocketApi {
  init(token) {
    this.socket = new W3CWebSocket(`${host}/ws`);
    this.socket.addEventListener('open', () => {
      console.log(`WS connected on ${host}`);
      const event = {
        data: token,
        type: 'connection',
      };
      this.socket.send(JSON.stringify(event));
    });

    this.socket.addEventListener('error', (error) => {
      console.error(`WS error: ${error}`);
    });

    this.socket.addEventListener('close', () => {
      console.log('WS disconnected');
    });
  }

  send(message) {
    try {
      const event = {
        type: 'message',
        data: message,
      };
      this.socket.send(JSON.stringify(event));
    } catch (error) {
      console.error(error);
    }
  }

  handleMessages(handler) {
    try {
      this.socket.addEventListener('message', (event) => handler(JSON.parse(event.data)));
    } catch (error) {
      return error;
    }
  }
}

export default new SocketApi();
