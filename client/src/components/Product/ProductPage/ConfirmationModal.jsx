import React from 'react';

const ConfirmationModal = ({ showPopup, forwardRef, handleRemove }) => (
  <div className="confirmation-modal-wrap" ref={forwardRef}>
    <div>Are you sure you want to remove this product?</div>
    <div className="confirmation-btns">
      <button className="remove-btn" onClick={handleRemove} type="button">
        Remove
      </button>
      <button className="cancel-btn" onClick={() => showPopup(false)} type="button">
        Cancel
      </button>
    </div>
  </div>
);

export default ConfirmationModal;
