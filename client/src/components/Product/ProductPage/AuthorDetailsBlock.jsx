import React, { useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import routes from 'src/routes/config';
import useAddToWishlist from 'src/utils/useAddToWishlist';
import Badge from '../../CustomComponents/Badge';
import Icon from '../../CustomComponents/Icon/Icon';
import Modal from '../../CustomComponents/Modal';
import ConfirmationModal from './ConfirmationModal';

const AuthorDetails = ({ product, matchUrl, user, handleRemove }) => {
  const isUserOwner = (user && product && product.owner.id) ? product.owner.id === user.id : false;
  const [isSaved, setSaved] = useAddToWishlist(user, product);
  const [isDeletePopupShown, setDeletePopupShown] = useState(false);
  const modalRef = useRef();

  const handleModalClick = (e) => {
    if (!e.target.closest(`.${modalRef.current.className}`)) {
      setDeletePopupShown(false);
    }
  };

  return (
    <div className="author-details">
      {isDeletePopupShown && (
        <Modal onClick={handleModalClick}>
          <ConfirmationModal
            showPopup={setDeletePopupShown}
            forwardRef={modalRef}
            handleRemove={handleRemove}
          />
        </Modal>
      )}
      <div className="author-info-wrap">
        <div className="green-block" />
        <div className="author-info">
          <Link to={`${routes.USER}/${product.owner.id}`} className="link-to-user">
            <Badge user={product.owner} wrapperClassName="badge-pdp" size="72px" fontSize="30px" />
          </Link>
          <Link to={`${routes.USER}/${product.owner.id}`} className="owner-name">
            {product.owner.fullName}
          </Link>
          <Link to={`/?location=${product.owner.location}`} className="location-pdp">
            {product.owner.location || ''}
          </Link>
        </div>
      </div>
      {isUserOwner ? (
        <>
          <div className="pdp-user-btn-wrap">
            <button
              onClick={() => setDeletePopupShown(true)}
              className="remove-btn btn"
              type="button"
            >
              <Icon name="trash" width="24" height="24" fill="white" />
              Remove product
            </button>
          </div>
          <div className="pdp-user-btn-wrap">
            <Link to={`${routes.PRODUCT}/${product.id}/edit`} className="update-btn btn">
              <Icon name="edit" width="24" height="24" fill="white" />
              Update product
            </Link>
          </div>
        </>
      ) : (
        <>
          <div className="pdp-user-btn-wrap">
            <Link to={{ pathname: matchUrl, search: '?chat=true' }} className="chat-btn btn">
              Chat with seller
            </Link>
          </div>

          <div>
            <button
              className="addToFavorite-btn btn"
              onClick={() => setSaved(isSaved)}
              type="button"
            >
              {isSaved ? (
                <>
                  <Icon name="heart_filled" height="17" width="16" fill="white" />
                  Remove favorite
                </>
              ) : (
                <>
                  <Icon name="heart" height="17" width="16" fill="white" />
                  Add to favorite
                </>
              )}
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default AuthorDetails;
