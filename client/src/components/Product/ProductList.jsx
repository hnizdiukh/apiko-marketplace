import React from 'react';
import Product from './ProductItem';
import Loading from '../CustomComponents/Loading/Loading';

const ProductList = ({ products }) => (
  <div className="product-list">
    {products
      ? products.map((p) => <Product product={p} key={p.id || p._id} />)
      : <Loading />}
  </div>
);

export default ProductList;
