import { Form, Formik } from 'formik';
import React from 'react';
import { Link } from 'react-router-dom';
import routes from 'src/routes/config';
import * as Yup from 'yup';
import Input from '../CustomComponents/InputComponent';

const ForgotPassword = ({ initialValues, onSubmit }) => (
  <>
    <div className="login-block">
      <h1>Restore password</h1>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={Yup.object().shape({
        email: Yup.string().email('Invalid email').required('Required'),
      })}
      >
        {({ values, handleChange, handleSubmit, isSubmitting }) => (
          <Form className="form" onSubmit={handleSubmit}>
            <Input
              onChange={handleChange}
              value={values.email}
              label="Email"
              type="email"
              name="email"
              autoComplete="email"
              placeholder="Example@gmail.com"
              required
            />

            <button className="primary-btn" type="submit" disabled={isSubmitting}>
              Restore
            </button>
          </Form>
        )}
      </Formik>
    </div>
    <div className="have-account" style={{ marginBottom: '0' }}>
      I already have an account, <Link to={routes.LOGIN}>Log In</Link>
    </div>
    <div className="have-account">
      I have no account, <Link to={routes.REGISTER}>Register now</Link>
    </div>
  </>
  );

export default ForgotPassword;
