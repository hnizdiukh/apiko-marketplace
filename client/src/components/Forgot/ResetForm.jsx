import { Form, Formik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import Input from '../CustomComponents/InputComponent';

const ResetForm = ({ initialValues, onSubmit }) => (
  <>
    <div className="login-block">
      <h1>Reset password</h1>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={Yup.object().shape({
          password: Yup.string().trim().min(8).required('Required'),
          confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Passwords do not match')
            .required('Password confirm is required')
      })}
      >
        {({ values, handleChange, handleSubmit, isSubmitting }) => (
          <Form className="form" onSubmit={handleSubmit}>
            <Input
              onChange={handleChange}
              value={values.password}
              label="New password"
              type="password"
              name="password"
              autoComplete="new-password"
              required
            />

            <Input
              onChange={handleChange}
              value={values.confirmPassword}
              label="Confirm password"
              type="password"
              name="confirmPassword"
              autoComplete="confirmPassword"
              required
            />

            <button className="primary-btn" type="submit" disabled={isSubmitting}>
              Save new password
            </button>
          </Form>
        )}
      </Formik>
    </div>
  </>
);

export default ResetForm;
