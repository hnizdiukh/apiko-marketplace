import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import store from 'src/store/createStore';
import { authOperations } from 'src/modules/auth';
import routes from 'src/routes/config';
import { Link } from 'react-router-dom';
import Loading from '../CustomComponents/Loading/Loading';
import ResetForm from './ResetForm';

import './forgot.css';

const ResetFormContainer = (props) => {
  const dispatch = useDispatch();
  const isError = useSelector((state) => state.auth.checkReset.isError);
  const isValidId = useSelector((state) => state.auth.checkReset.isSuccess);

  useEffect(() => {
    (async () => {
      await dispatch(authOperations.checkReset({ id: props.match.params.id }));
    })();
  }, [dispatch, props.match.params.id]);

  const initialValues = { password: '', confirmPassword: '' };

  const onSubmit = async (values) => {
    await dispatch(authOperations.reset({ id: props.match.params.id, password: values.password }));
    const storeState = await store.getState();
    const { isSuccess, error } = storeState.auth.reset;

    if (isSuccess) {
      toast.success('Password was changed');
      props.history.push(routes.LOGIN);
    } else {
      toast.error(error.message);
    }
  };

  const Login = !isValidId && !isError ? (
    <Loading />
  ) : (
    isError ? (
      <div className="login-block invalid">
        <p>Password recovery URL has expired.</p>
        <Link to={routes.LOGIN}>Log in</Link>
        <Link to={routes.FORGOT}>Restore password</Link>
      </div>
    ) : <ResetForm initialValues={initialValues} onSubmit={onSubmit} />
  );

  return Login;
};

export default ResetFormContainer;
