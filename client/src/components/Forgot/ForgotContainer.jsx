import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import store from 'src/store/createStore';
import { authOperations } from 'src/modules/auth';
import routes from 'src/routes/config';
import Loading from '../CustomComponents/Loading/Loading';
import ForgotPassword from './ForgotPassword';

const ForgotFormContainer = (props) => {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.auth.login.isLoading);

  const initialValues = { email: '' };

  const onSubmit = async (values) => {
    await dispatch(authOperations.forgot({ email: values.email }));
    const storeState = await store.getState();
    const { isSuccess } = storeState.auth.forgot;

    if (isSuccess) {
      toast.success('Password recovery email sent');
      props.history.push(routes.LOGIN);
    } else {
      toast.error('Incorrect email!');
    }
  };

  const Login = isLoading ? (
    <Loading />
  ) : (
    <ForgotPassword initialValues={initialValues} onSubmit={onSubmit} />
  );

  return Login;
};

export default ForgotFormContainer;
