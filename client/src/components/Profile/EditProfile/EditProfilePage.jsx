import React, { useRef, useState } from 'react';
import { Formik, Form } from 'formik';
import Input from 'src/components/CustomComponents/InputComponent';
import Badge from 'src/components/CustomComponents/Badge';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';
import { viewerOperations } from 'src/modules/viewer';
import Api from 'src/api';
import routes from 'src/routes/config';
import { useHistory } from 'react-router-dom';

const EditProfile = ({ user, hanldeImageClick, initialValues, schema }) => {
  const dispatch = useDispatch();
  const imageInput = useRef(false);
  const history = useHistory();

  const [imageSelected, setImageSelected] = useState(null);

  const hanldeImageUpload = () => {
    setImageSelected(imageInput.current.files[0]);
  };

  const onSubmit = async (values) => {
    if (user.fullName === values.fullName && user.phone === values.phone && !imageSelected) {
      toast.success('Profile updated!');
      return;
    }

    try {
      let res;
      if (imageSelected) {
        try {
          res = await Api.Image.upload(imageSelected);
        } catch (error) {
          throw new Error('Image can not be uploaded!');
        }
      }

      await dispatch(
        viewerOperations.putUser({
          fullName: values.fullName,
          phone: values.phone || null,
          avatar: res ? res.data?.data?.link : user.avatar || '',
          location: values.location || user.location,
        })
      );
      toast.success('Profile updated!');
      history.push(routes.HOME);
    } catch (error) {
      toast.error(error.message || 'Can not update the profile');
    }
  };

  return (
    <>
      <main className="container">
        <div className="profile-block">
          <h1>Edit profile</h1>
          {imageSelected ? (
            <div className="selected-image">
              <img src={URL.createObjectURL(imageSelected)} alt="imageSelected.name" />
            </div>
          ) : (
            <Badge
              user={user}
              onClick={hanldeImageClick}
              wrapperClassName="profile-page-badge"
              size="100px"
              fontSize="40px"
            />
          )}

          <div className="upgrade-photo-wrap">
            {imageSelected ? (
              <button
                className="upgrade-photo upgrade-photo-selected"
                onClick={() => setImageSelected()}
                type="button"
              >
                Deselect
              </button>
            ) : (
              <button
                onClick={() => {
                  imageInput.current.click();
                }}
                className="upgrade-photo"
                type="button"
              >
                Upgrade photo
              </button>
            )}
            <input
              type="file"
              accept="image/*"
              ref={imageInput}
              onChange={hanldeImageUpload}
              alt="image"
              name="profile-image"
            />
          </div>

          <Formik initialValues={initialValues} validationSchema={schema} onSubmit={onSubmit}>
            {({ values, handleChange, handleSubmit, isSubmitting }) => (
              <Form className="form" onSubmit={handleSubmit}>
                <Input
                  onChange={handleChange}
                  value={values.fullName}
                  label="Full name"
                  type="text"
                  name="fullName"
                  autoComplete="name"
                  placeholder="Tony Stark"
                />

                <Input
                  onChange={handleChange}
                  value={values.phone}
                  label="Phone number"
                  type="phone"
                  name="phone"
                  autoComplete="tel"
                  placeholder="+380 12 345 678"
                />

                <Input
                  onChange={handleChange}
                  value={values.location}
                  label="Location"
                  type="text"
                  name="location"
                  autoComplete="address-level2"
                  placeholder="Los Angeles, CA"
                />

                <button className="primary-btn" type="submit" disabled={isSubmitting}>
                  Save
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </main>
    </>
  );
};

export default EditProfile;
