import React from 'react';
import Badge from 'src/components/CustomComponents/Badge';
import { Link } from 'react-router-dom';
import ProductList from 'src/components/Product/ProductList';

const Profile = ({ user, userProducts, matchUrl, isCurrentUser }) => {
  const products = userProducts.list || [];

  return (
    <main className="white-container">
      <div className="center profile-info">
        <Badge user={user} size="100px" fontSize="50px" />
        <p className="owner-name">{user.fullName}</p>
        <Link to={`/?location=${user.location}`} className="location-pdp">
          {user.location || ''}
        </Link>
        {isCurrentUser && (
          <Link to={{ pathname: matchUrl, search: '?chat=true' }} className="chat-btn btn">
            Send message
          </Link>
        )}
      </div>
      <h1 className="items-title ml-20">
        {products.length ? (
          <>
            User products <span>({products.length})</span>
          </>
        ) : (
          <>
            <p className="center">The user is not currently selling any product</p>
          </>
        )}
      </h1>
      <ProductList products={products} />
    </main>
  );
};

export default Profile;
