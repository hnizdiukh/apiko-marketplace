import React from 'react';
import ProductList from '../Product/ProductList';

const WishlistPage = ({ products }) => (
  <main className="white-container">
    <div className="product-container">
      {products.length ? (
        <h1 className="items-title">
          <>
            Saved items <span>({products.length})</span>
          </>
        </h1>
      ) : (
        <h2 className="not-found center" style={{ height: '65vh' }}>
          You do not have any saved product
        </h2>
      )}
      <ProductList products={products} />
    </div>
  </main>
);

export default WishlistPage;
