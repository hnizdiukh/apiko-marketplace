import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { productsOperations } from 'src/modules/products';
import Api from 'src/api';
import Loading from '../CustomComponents/Loading/Loading';
import WishlistPage from './WishlistComponent';
import UnloginedRedirect from '../CustomComponents/Unlogined';

const Wishlist = () => {
  const dispatch = useDispatch();
  const { isLoggedIn } = Api.Auth;
  const user = useSelector((state) => state.viewer.user);
  const products = useSelector((state) => state.products.saved);

  useEffect(() => {
    if (user) dispatch(productsOperations.savedProducts());
  }, [user, dispatch]);

  if (!isLoggedIn) {
    return <UnloginedRedirect />;
  }

  const WishlistElement = !user
    ? <Loading />
    : <WishlistPage products={products} />;

  return WishlistElement;
};

export default Wishlist;
