import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import store from 'src/store/createStore';
import { authOperations } from 'src/modules/auth';
import { LoginSchema } from 'src/configs/schemas';
import routes from 'src/routes/config';
import Loading from '../CustomComponents/Loading/Loading';
import LoginForm from './LoginFormComponent';

const LoginFormContainer = (props) => {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.auth.login.isLoading);

  const initialValues = { email: '', password: '' };
  const onSubmit = async (values) => {
    await dispatch(authOperations.login({ email: values.email, password: values.password }));
    const storeState = await store.getState();
    const { isSuccess } = storeState.auth.login;

    if (isSuccess) {
      props.history.push(props.location.state ? props.location.state.prevPath : routes.HOME);
    } else {
      toast.error('Incorrect email or password!');
    }
  };

  const Login = isLoading ? (
    <Loading />
  ) : (
    <LoginForm schema={LoginSchema} initialValues={initialValues} onSubmit={onSubmit} />
  );

  return Login;
};

export default LoginFormContainer;
