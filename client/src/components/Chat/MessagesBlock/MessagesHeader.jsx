import React from 'react';
import Icon from 'src/components/CustomComponents/Icon/Icon';
import routes from 'src/routes/config';
import { Link } from 'react-router-dom';
import Badge from 'src/components/CustomComponents/Badge';

const MessagesHeader = ({ setChatListShown, chat, imageSrc, userId }) => {
  const chatWith = chat.participants.find((u) => u.id !== userId);
  return (
    <div className="messages-header">
      <span
        className="back-btn"
        onClick={() => setChatListShown(true)}
        onKeyPress={() => setChatListShown(true)}
        role="button"
        tabIndex={0}
      >
        <Icon name="next" width="30" height="35" />
      </span>
      <Link to={`${routes.USER}/${chatWith.id}`}>
        <Badge user={chatWith} size="45px" />
        <div className="messaging-with">{chatWith.fullName}</div>
      </Link>
      <Link to={`${routes.PRODUCT}/${chat.product.id}`} className="chat-product">
        <span className="product-image">
          <img src={imageSrc} alt={chat.product.title} />
        </span>
        <div className="chat-product-details">
          <span className="chat-product-name">{chat.product.title}</span>
          <span className="chat-product-price">${chat.product.price.toFixed(2)}</span>
        </div>
        <span className="product-link">
          <Icon name="open_link" width="18" height="18" />
        </span>
      </Link>
    </div>
  );
};

export default MessagesHeader;
