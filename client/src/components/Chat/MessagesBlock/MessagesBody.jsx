import React, { useCallback, useEffect, useRef, useState } from 'react';
import Moment from 'react-moment';
import { useDispatch, useSelector } from 'react-redux';
import SimpleBar from 'simplebar-react';
import Loading from 'src/components/CustomComponents/Loading/Loading';
import { messagesOperations } from 'src/modules/messages';

const MessagesBody = ({ windowSize, chatId,
                        messageInput, isMessagesLoading, userId }) => {
  const dispatch = useDispatch();

  const messagesEndRef = useRef(null);

  const messages = useSelector((state) => state.messages.messages);
  const messagesLeft = useSelector((state) => state.messages.messagesLeft);
  const isLoading = useSelector((state) => state.messages.fetchMessages.isLoading);
  const messagesTriggerCount = useSelector((state) => state.messages.messagesTriggerCount);

  const [loadedChats, setLoadedChats] = useState([]);
  const [page, setPage] = useState(0);
  const [wasScrolled, setWasScrolled] = useState(false);

  const handleScroll = useCallback((e) => {
    if (e.target.scrollTop <= 150 && messagesLeft && !isLoading) {
      setPage((prevPage) => prevPage + 1);
    }
  }, [messagesLeft, isLoading]);

  useEffect(() => {
    if (messagesEndRef.current
        && !messageInput
        && !isMessagesLoading
        && !wasScrolled
        && messages.has(chatId)
      ) {
      messagesEndRef.current.scrollIntoView();
      setWasScrolled(true);
    }
  }, [messageInput, isMessagesLoading, wasScrolled, messages, chatId, messagesTriggerCount]);

  useEffect(() => {
    setPage(0);
    setWasScrolled(false);
  }, [chatId]);

  useEffect(() => {
    if (page === 0) messagesEndRef.current.scrollIntoView();
  }, [messagesTriggerCount, page]);

  useEffect(() => {
    if ((page === loadedChats.filter((id) => id === chatId).length)) {
      dispatch(messagesOperations.fetchMessages(
        { chatId, offset: 20 * page, limit: 20 }
      ));
      setLoadedChats((prev) => [...prev, chatId]);
    }
  }, [page, chatId, loadedChats, dispatch]);

  return (
    <div className="messages-body">
      <SimpleBar
        style={{ maxHeight: windowSize.height - 212 }}
        autoHide={false}
        scrollableNodeProps={{
          onScroll: handleScroll
        }}
      >
        {messages.has(chatId) ? (
          <>
            {isLoading && <Loading />}
            {messages.get(chatId).map((message) => (
              <div
                className={userId !== message.ownerId ? 'message-recieved' : 'message-send'}
                key={`${message.createdAt}`}
              >
                <div className="message-text">{message.text}</div>
                <div className="message-time-under">
                  <Moment fromNow>{message.createdAt}</Moment>
                </div>
              </div>
            ))}
          </>
        ) : ''}
        <span ref={messagesEndRef} />
      </SimpleBar>
    </div>
  );
};

export default MessagesBody;
