import React, { useRef } from 'react';
import Modal from 'src/components/CustomComponents/Modal';
import { useDispatch, useStore, useSelector } from 'react-redux';
import routes from 'src/routes/config';
import { chatOperations } from 'src/modules/chat';
import { messagesOperations } from 'src/modules/messages';
import { toast } from 'react-toastify';
import { ChatModalSchema } from 'src/configs/schemas';
import ChatModal from './ChatModal';
import '../chat.css';

const ChatModalContainer = (props) => {
  const { location, history } = props;
  const params = new URLSearchParams(location.search);
  const chatModalRef = useRef();
  const store = useStore();
  const dispatch = useDispatch();
  const product = useSelector((state) => state.products.product);

  const handleModalClick = (e) => {
    if (!e.target.closest(`.${chatModalRef.current.className}`) || e.target.closest('.close-btn')) {
      history.push(location.pathname);
    }
  };

  const initialValues = {
    text: '',
  };

  const handleMessageSubmit = async (values) => {
    let createdChat;
    try {
      if (!product.chatId) {
        await dispatch(chatOperations.createChat(product.id));
        const storeState = await store.getState();
        createdChat = storeState.chat.createdChat;
      }
      await dispatch(messagesOperations.sendMessage(values, createdChat || product.chatId));
      if (!createdChat && !product.chatId) {
        throw new Error('Imposible to create chat');
      }
      await props.history.push(`${routes.CHAT}/${createdChat || product.chatId || ''}`);
    } catch (error) {
      toast.error('Imposible to create chat');
    }
  };

  return (
    params.get('chat')
    && product && (
      <Modal
        onClick={(e) => {
          handleModalClick(e);
        }}
      >
        <ChatModal
          initialValues={initialValues}
          schema={ChatModalSchema}
          handleMessageSubmit={handleMessageSubmit}
          forwardRef={chatModalRef}
          product={product}
        />
      </Modal>
    )
  );
};

export default ChatModalContainer;
