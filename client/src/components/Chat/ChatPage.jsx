import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Api from 'src/api';
import { chatOperations } from 'src/modules/chat';
import { toast } from 'react-toastify';
import SimpleBar from 'simplebar-react';
import useWindowSize from 'src/utils/useWindowSize';
import routes from 'src/routes/config';
import Messages from './MessagesBlock/MessagesContainer';
import ChatItem from './ChatItem';
import UnloginedRedirect from '../CustomComponents/Unlogined';

import 'simplebar/dist/simplebar.min.css';
import Loading from '../CustomComponents/Loading/Loading';

const ChatPage = ({ match, history }) => {
  const dispatch = useDispatch();
  const { isLoggedIn } = Api.Auth;
  const windowSize = useWindowSize();
  const chats = useSelector((state) => state.chat.chats);
  const isChatsLoading = useSelector((state) => state.chat.fetchChats.isLoading);
  const messages = useSelector((state) => state.messages.messages);
  const user = useSelector((state) => state.viewer.user);
  const [isChatListShown, setChatListShown] = useState(false);

  useEffect(() => {
    try {
      dispatch(chatOperations.fetchChats());
    } catch (error) {
      toast.error(error.message);
    }
  }, [dispatch]);

  useEffect(() => {
    try {
      if (match.params.chatId) {
        setChatListShown(false);
      } else {
        setChatListShown(true);
      }
    } catch (error) {
      history.push(routes.CHAT);
      toast.error(error.message);
    }
  }, [dispatch, match.params.chatId, history, messages]);

  if (!isLoggedIn) {
    return <UnloginedRedirect />;
  }

  if (isChatsLoading || !user) {
    return <Loading />;
  }

  if (!chats.length) {
    return <div className="not-found">You don&apos;t have any chats yet</div>;
  }

  const chatSelected = chats.find((c) => c.id === match.params.chatId);

  return (
    <div className="chat-page" style={{ height: windowSize.height - 78 }}>
      <div className="chat-list-wrap" style={{ display: isChatListShown ? 'block' : '' }}>
        <div className="chat-list">
          <SimpleBar
            style={{ maxHeight: windowSize.height - 78 }}
          >
            {chats.length
              ? chats.map((chat) => (
                <ChatItem
                  chat={chat}
                  key={chat.id}
                  userId={user.id}
                  current={match.params.chatId}
                  setChatListShown={setChatListShown}
                />
                ))
              : null}
          </SimpleBar>
        </div>
      </div>
      <Messages
        chat={chatSelected}
        setChatListShown={setChatListShown}
        isChatListShown={isChatListShown}
      />
    </div>
  );
};

export default ChatPage;
