import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { productsOperations } from 'src/modules/products';
import './productlist.css';
import { useLocation } from 'react-router-dom';
import Loading from '../CustomComponents/Loading/Loading';
import Homepage from './HomepageComponent';
import { PRODUCT_LIMIT } from 'src/configs/variables';

const HomepageContainer = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const user = useSelector((state) => state.viewer.user);
  const products = useSelector((state) => state.products.products);
  const isLoading = useSelector((state) => state.products.fetchProducts.isLoading);
  const isSearching = useSelector((state) => state.products.search.isLoading);
  const isLoadingMore = useSelector((state) => state.products.fetchMoreProducts.isLoading);
  const isUpdating = useSelector((state) => state.products.updateProduct.isLoading);

  useEffect(() => {
    if (user) dispatch(productsOperations.savedProducts());
  }, [user, dispatch]);

  useEffect(() => {
    if (location.search) {
      dispatch(productsOperations.search(new URLSearchParams(location.search)));
    } else {
      dispatch(productsOperations.fetchProducts());
    }
  }, [dispatch, location]);

  const hanldeLoadMore = () => {
    const offset = products.length;
    dispatch(productsOperations.fetchMoreProducts({ offset }));
  };

  let HomepageElement =
    isLoading || isSearching || isUpdating ? (
      <Loading />
    ) : (
      <Homepage
        products={products}
        hanldeLoadMore={hanldeLoadMore}
        isLoadingMore={isLoadingMore}
        limit={PRODUCT_LIMIT}
        search={location.search}
      />
    );

  if (!products.length && !isLoading && !isSearching) {
    HomepageElement = <div className="no-products not-found">No products found</div>;
  }

  return HomepageElement;
};

export default HomepageContainer;
