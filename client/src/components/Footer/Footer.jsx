import React from 'react';
import './footer.css';
import { Link, useLocation, matchPath } from 'react-router-dom';
import routes from 'src/routes/config';

const Footer = () => {
  const location = useLocation();

  if (
    matchPath(location.pathname, {
      path: routes.CHAT_ID,
      exact: true,
      strict: false,
    })
  ) {
    return null;
  }

  return (
    <footer>
      <div>Copyright &copy; 2020.</div>
      <Link to={routes.PRIVACY}>Privacy Policy.</Link>
    </footer>
  );
};

export default Footer;
