import React from 'react';
import './sellproduct.css';
import ProductForm from '../CustomComponents/Forms/ProductForm';

const SellProduct = ({ initialValues, schema, onSubmit, handleImageUpload }) => (
  <main className="container">
    <div className="sell-container">
      <div className="sell-wrap">
        <h1>Add product</h1>
        <ProductForm
          initialValues={initialValues}
          schema={schema}
          onSubmit={onSubmit}
          handleImageUpload={handleImageUpload}
        />
      </div>
    </div>
  </main>
);

export default SellProduct;
