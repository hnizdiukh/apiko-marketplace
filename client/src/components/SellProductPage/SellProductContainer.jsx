import React, { useState } from 'react';
import Api from 'src/api';
import { useDispatch } from 'react-redux';
import { productsOperations } from 'src/modules/products';
import { useHistory } from 'react-router-dom';
import routes from 'src/routes/config';
import { toast } from 'react-toastify';
import { ProductSchema } from 'src/configs/schemas';
import SellProduct from './SellProduct';
import UnloginedRedirect from '../CustomComponents/Unlogined';

const SellProductPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isLoggedIn } = Api.Auth;

  const initialValues = {
    title: '',
    location: '',
    description: '',
    price: '',
  };

  const [images, setImages] = useState([]);

  const fetchImage = async (image) => {
    const res = await Api.Image.upload(image);
    return res.data.data.link;
  };

  const getImages = async (imgs) => Promise.all(imgs.map((image) => fetchImage(image)));

  const handleAddProduct = async (values) => {
    try {
      const { title, location, description, price } = values;
      let imagesResponsesArray;
      const imageLinksArray = [];

      if (images) {
        imagesResponsesArray = await getImages(images);
      }

      if (imagesResponsesArray && Array.isArray(imagesResponsesArray)) {
        imagesResponsesArray.map((imageRes) => imageLinksArray.push(imageRes));
      }

      imageLinksArray.forEach((link) => {
        if (typeof link !== 'string' || !link.length) {
          throw new Error('Images are not uploaded');
        }
      });

      const product = {
        title,
        description,
        photos: imageLinksArray || [],
        location,
        price,
      };

      await dispatch(productsOperations.addProduct(product));
      history.push(routes.HOME);
    } catch (error) {
      toast.error(error.message || 'Can not upload this product');
    }
  };

  const handleImageUpload = (imgs) => {
    setImages(imgs);
  };

  if (!isLoggedIn) {
    return <UnloginedRedirect />;
  }

  return (
    <SellProduct
      initialValues={initialValues}
      schema={ProductSchema}
      onSubmit={handleAddProduct}
      handleImageUpload={handleImageUpload}
    />
  );
};

export default SellProductPage;
