import React from 'react';

const Item = ({ index, src, current, productName }) => {
  const classNames = [];
  classNames.push(current === index ? 'current' : '', 'slider-item');

  return <img src={src} alt={productName} className={classNames.join(' ')} />;
};

export default Item;
