import React from 'react';

import IconsConfig from './IconsConfig';

const Icon = ({ name, ...props }) => {
  const IconC = IconsConfig[name];
  return <IconC {...props} />;
};

export default Icon;
