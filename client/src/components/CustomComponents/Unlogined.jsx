import React from 'react';
import { Redirect, useLocation } from 'react-router-dom';
import routes from 'src/routes/config';

const UnloginedRedirect = () => {
  const location = useLocation();
  return <Redirect to={{ pathname: routes.LOGIN, state: { prevPath: location.pathname } }} />;
};

export default UnloginedRedirect;
