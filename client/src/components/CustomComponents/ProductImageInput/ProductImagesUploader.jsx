/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Fragment, useRef, useState, useEffect } from 'react';
import './productImageInput.css';

const renderButtons = (images, setImages, imageInput) => {
  const arrayOfButtons = [];

  if (images.length > 5) {
    return [];
  }
  for (let i = 0; i < images.length + 1; i += 1) {
    arrayOfButtons.push(
      <Fragment key={i}>
        <div
          onClick={() => imageInput.current.click()}
          className="upload-product-image"
          role="button"
          key="tab"
          aria-hidden="true"
        >
          +
        </div>
        <input
          onChange={(e) => {
            if (e.target.files[0]) {
              setImages([...images, e.target.files[0]]);
            }
          }}
          type="file"
          accept="image/*"
          name="photos"
          autoComplete="photo"
          className="product-image-input"
          ref={imageInput}
        />
      </Fragment>
    );

    return arrayOfButtons;
  }
};

const ProductImagesInput = ({ handleImageUpload, value }) => {
  const imageInput = useRef();
  const [images, setImages] = useState(value || []);

  useEffect(() => {
    handleImageUpload(images);
  }, [images, handleImageUpload]);

  return (
    <>
      <label>Photos</label>
      <div className="product-image-container">
        {images.map((image) => (
          <div key={image.lastModified || image}>
            <img
              className="uploaded-product-image"
              src={typeof image === 'object' ? URL.createObjectURL(image) : image}
              alt={image.name}
            />
            <span
              className="remove-image"
              onClick={() => setImages(images.filter((img) => img !== image))}
              onKeyPress={() => setImages(images.filter((img) => img !== image))}
              role="button"
              tabIndex={0}
            >
              -
            </span>
          </div>
        ))}
        {renderButtons(images, setImages, imageInput)}
      </div>
    </>
  );
};

export default ProductImagesInput;
