import React from 'react';
import './loading.css';

const Loading = ({ height }) => (
  <div className="loading" style={{ height }}>
    <div className="lds-dual-ring" />
  </div>
);

export default Loading;
