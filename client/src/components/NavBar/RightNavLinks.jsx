import React from 'react';
import { NavLink } from 'react-router-dom';
import Icon from 'src/components/CustomComponents/Icon/Icon';
import routes from 'src/routes/config';
import AccountDropdown from './Account/AccountDropdown';

const RightNavLinks = ({ isWhiteNavbar, user, onLogout, location }) => (
  <div className="right-nav-btns">
    {isWhiteNavbar ? (
      ''
    ) : (
      <NavLink to={routes.CHAT} className="chat-link">
        <Icon name="chat" height="18" width="18" />
      </NavLink>
    )}
    <NavLink to={routes.SELL} className="btn sell-btn">
      Sell
    </NavLink>
    {user ? (
      <>
        <AccountDropdown user={user} onLogout={onLogout} />
      </>
    ) : (
      <>
        <NavLink
          to={{ pathname: routes.LOGIN, state: { prevPath: location.pathname } }}
          className="btn-login"
          style={{ color: isWhiteNavbar ? '#2B2B2B' : '#FFFFFF' }}
        >
          Login
        </NavLink>
      </>
    )}
    <NavLink to={routes.WISH_LIST} className="wishlist-link">
      {location.pathname === routes.WISH_LIST ? (
        <Icon name="heart_filled" height="17" width="16" fill="#FFFFFF" />
      ) : (
        <Icon name="heart" height="17" width="16" fill={isWhiteNavbar ? '#2E2E2E' : '#FFFFFF'} />
      )}
    </NavLink>
  </div>
);

export default RightNavLinks;
