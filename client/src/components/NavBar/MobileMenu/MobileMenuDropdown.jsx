import React, { useEffect, useState, useRef } from 'react';
import Icon from 'src/components/CustomComponents/Icon/Icon';
import { NavLink } from 'react-router-dom';
import routes from 'src/routes/config';
import './mobile-menu.css';

const MobileMenu = ({ isWhiteNavbar }) => {
  const [isShown, setShown] = useState(false);
  const menuRef = useRef();
  const menuButtonRef = useRef();

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        menuRef.current
        && menuButtonRef.current
        && !menuRef.current.contains(event.target)
        && !menuButtonRef.current.contains(event.target)
      ) {
        setShown(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isShown, menuRef]);

  return (
    <>
      <span
        className="hamburger-icon"
        onClick={() => setShown(!isShown)}
        ref={menuButtonRef}
        onKeyPress={() => setShown(!isShown)}
        role="button"
        tabIndex={0}
      >
        <Icon name="hamburger" height="32" width="32" fill={isWhiteNavbar ? 'black' : 'white'} />

        <div className={isShown ? 'mobile-menu' : 'hide'} ref={menuRef}>
          <NavLink to={routes.CHAT} className="chat-link-mobile">
            Chat
          </NavLink>
          <NavLink to={routes.SELL} className="chat-link-mobile">
            Sell product
          </NavLink>
          <NavLink to={routes.WISH_LIST} className="wishlist-link-mobile">
            Wishlist
          </NavLink>
        </div>
      </span>
    </>
  );
};

export default MobileMenu;
