/* eslint-disable no-restricted-syntax */
import React, { useState, useEffect } from 'react';
import { NavLink, useLocation, useHistory, matchPath } from 'react-router-dom';
import routes from 'src/routes/config';
import Icon from 'src/components/CustomComponents/Icon/Icon';
import { useDispatch, useSelector } from 'react-redux';
import { productsOperations } from 'src/modules/products';
import { toast } from 'react-toastify';
import Search from './SearchForm/SearchForm';
import Filters from './Filters/Filters';
import MobileMenu from './MobileMenu/MobileMenuDropdown';
import RightNavLinks from './RightNavLinks';

const NavBar = ({ user, onLogout }) => {
  const pageLocation = useLocation();
  const dispatch = useDispatch();
  const history = useHistory();

  const searchParams = useSelector((state) => state.products.search.searchParams);

  const initialSearchValues = { priceFrom: '', priceTo: '', location: '', keywords: '' };

  const [searchValues, setSearchValues] = useState(initialSearchValues);

  const pagesWithSearchBar = [routes.HOME, routes.WISH_LIST, routes.PRODUCT_ID];

  const arrayOfMatched = pagesWithSearchBar.map((link) =>
    matchPath(pageLocation.pathname, {
      path: link,
      exact: true,
      strict: false,
    }));

  const pagesWithWhiteNavbar = [routes.LOGIN, routes.REGISTER];
  let classNames;
  const isWhiteNavbar = pagesWithWhiteNavbar.includes(pageLocation.pathname);

  const isSearchShown = !arrayOfMatched.every((m) => m === null);
  const isFilterShown = pageLocation.pathname === routes.HOME;

  if (isWhiteNavbar) {
    classNames = 'navbar';
  } else {
    classNames = 'navbar main-navbar';
  }

  function paramsToObject(entries) {
    const result = {};
    for (const entry of entries) {
      const [key, value] = entry;
      result[key] = value;
    }
    return result;
  }

  useEffect(() => {
    if (searchParams) {
      setSearchValues((prevValues) => ({
        ...prevValues,
        ...paramsToObject(searchParams.entries()),
      }));
    }
  }, [searchParams]);

  const handleSearch = (e) => {
    e.preventDefault();
    const { keywords, location } = searchValues;
    let { priceFrom, priceTo } = searchValues;
    priceFrom = Number(priceFrom);
    priceTo = Number(priceTo);

    if (!keywords.trim() && !location.trim() && !priceFrom && !priceTo) {
      return toast.error('Please enter product name or location');
    }

    if ((typeof priceFrom !== 'number' || typeof priceTo !== 'number') && priceFrom && priceTo) {
      return toast.error('Price fields have to be filled with numbers!');
    }
    const usp = new URLSearchParams();
    if (keywords.trim()) {
      usp.append('keywords', keywords);
    }
    if (location.trim()) {
      usp.append('location', location);
    }
    if (priceFrom) {
      usp.append('priceFrom', priceFrom);
    }
    if (priceTo) {
      usp.append('priceTo', priceTo);
    }
    history.push(`${routes.HOME}?${usp}`);
    dispatch(productsOperations.search(usp));
  };

  return (
    <>
      <nav className={classNames}>
        <div className="top-navbar">
          <MobileMenu isLoggedIn={!!user} isWhiteNavbar={isWhiteNavbar} />
          <NavLink to={routes.HOME}>
            <Icon name={isWhiteNavbar ? 'logo' : 'logo_white'} height="42" width="102" />
          </NavLink>
          <RightNavLinks
            isWhiteNavbar={isWhiteNavbar}
            user={user}
            onLogout={onLogout}
            location={pageLocation}
          />
        </div>
        {isSearchShown ? (
          <Search
            handleSearch={handleSearch}
            searchValues={searchValues}
            setSearchValues={setSearchValues}
          />
        ) : (
          ''
        )}
      </nav>
      {isFilterShown ? (
        <Filters
          handleSearch={handleSearch}
          searchValues={searchValues}
          setSearchValues={setSearchValues}
          initialSearchValues={initialSearchValues}
        />
      ) : (
        ''
      )}
    </>
  );
};

export default NavBar;
