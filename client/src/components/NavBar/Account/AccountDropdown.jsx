import React, { useState, useEffect, useRef } from 'react';
import { NavLink, useHistory, useLocation } from 'react-router-dom';
import routes from 'src/routes/config';
import Badge from '../../CustomComponents/Badge';
import './account-dropdown.css';

const AccountDropdown = ({ user, onLogout }) => {
  const [isDropdownShown, setDropdownShown] = useState(false);
  const accountMenuRef = useRef();
  const history = useHistory();
  const location = useLocation();

  const onClick = () => {
    setDropdownShown(!isDropdownShown);
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (accountMenuRef.current && !accountMenuRef.current.contains(event.target)) {
        setDropdownShown(false);
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isDropdownShown, accountMenuRef]);

  return (
    <>
      <Badge user={user} onClick={onClick} wrapperClassName="badge-toggler" size="40px" />
      <div
        className="account-menu"
        style={{ display: isDropdownShown ? 'flex' : 'none' }}
        ref={accountMenuRef}
      >
        <span className="account-dropdown-row">
          <span className="avatar-wrap">
            <Badge
              user={user}
              size="40px"
              onClick={() => {
                setDropdownShown(false);
                return history.push(routes.PROFILE);
              }}
            />
          </span>
          <span className="account-dropdown-col">
            <b className="account-dropdown-fullName">{user.fullName}</b>
            <p className="account-dropdown-email">{user.email}</p>
            <NavLink
              to={routes.PROFILE}
              className="profile-small-link"
              onClick={() => setDropdownShown(false)}
            >
              Profile
            </NavLink>
          </span>
        </span>
        <NavLink
          to={routes.EDIT_PROFILE}
          className="profile-link"
          onClick={() => setDropdownShown(false)}
        >
          Edit Profile
        </NavLink>
        <hr />
        <NavLink
          to={{ pathname: routes.LOGIN, state: { prevPath: location.pathname } }}
          className="profile-link"
          onClick={onLogout}
        >
          Logout
        </NavLink>
      </div>
    </>
  );
};

export default AccountDropdown;
