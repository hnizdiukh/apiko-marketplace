# 8th-react-intensive-volodymyr-hnizdiukh

Deployed to: https://hnizdiukh-marketplace.herokuapp.com/

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### To run client locally:

1. Navigate to client folder with `cd client`
2. Run `npm install` to install all dependencies
3. Run `npm start-client` to start the application client.

### To run server follow these steps:

1. Run `npm run build` to build client
2. `npm install`
3. `npm install -g ts-node`
4. Create `.env` from `.env.example`
5. `npm run build-server` to compile TypeScript files
6. `npm run start` or `npm run dev` to run server with client
