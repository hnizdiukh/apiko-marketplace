"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeProduct = exports.unsaveProduct = exports.saveProduct = exports.userProducts = exports.searchProducts = exports.savedProducts = exports.updateProduct = exports.getSingleProduct = exports.postProduct = exports.getProducts = void 0;
const slugify_1 = __importDefault(require("slugify"));
const schemas_1 = require("../schemas");
const db_1 = __importDefault(require("../db"));
const getUserFromJWT_1 = __importDefault(require("../utils/getUserFromJWT"));
const validateAuthHeaders_1 = __importDefault(require("../utils/validateAuthHeaders"));
const productCollection = db_1.default.get('products');
const savedProductCollection = db_1.default.get('saved_products');
const usersCollection = db_1.default.get('users');
exports.getProducts = async (req, res, next) => {
    try {
        const { body } = req;
        const { params } = req;
        const limit = (body && body.value) ? body.value.limit : (params.limit || 20);
        const offset = (body && body.value) ? body.value.offset : (params.offset || 0);
        const products = await productCollection.aggregate([
            { $sort: { createdAt: -1 } },
            { $skip: Number(offset) },
            { $limit: Number(limit) },
        ]);
        const token = await validateAuthHeaders_1.default(req);
        const currentUser = token ? await getUserFromJWT_1.default(token) : null;
        const runAsyncFunctions = async (user) => {
            if (!user) {
                products.map((product) => (product.saved = false));
                return;
            }
            let saved;
            await Promise.all(products.map(async (product) => {
                saved = await savedProductCollection.find({ userId: user.id, productIds: product.id });
                product.saved = !!saved.length;
            }));
        };
        await runAsyncFunctions(currentUser);
        if (!products) {
            throw new Error('No products found');
        }
        res.status(200);
        res.send(products);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.postProduct = async (req, res, next) => {
    try {
        const { body } = req;
        const product = (await schemas_1.productSchema.validate(body, { stripUnknown: true }));
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        const slug = slugify_1.default(product.title, { lower: true, strict: true, locale: 'en' });
        product.id = `${slug}-${(Date.now() % 1000) + 1}`;
        product.createdAt = Date.now();
        product.ownerId = user.id;
        await productCollection.insert(product);
        res.json(product);
        res.status(201);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.getSingleProduct = async (req, res, next) => {
    const { id } = req.params;
    try {
        const product = await productCollection.findOne({ id });
        if (!product) {
            throw new Error(`No product with such id ${id}`);
        }
        const owner = await usersCollection.findOne({ id: product.ownerId });
        const { password, ...ownerResponse } = owner;
        if (!owner) {
            throw new Error('Product owner not found');
        }
        product.owner = ownerResponse;
        res.json(product);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.updateProduct = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { body } = req;
        const product = await productCollection.findOne({ id });
        if (!product) {
            throw new Error(`No product with such id ${id}`);
        }
        const productToUpdate = (await schemas_1.productSchema.validate(body, { stripUnknown: true }));
        productToUpdate.updatedAt = Date.now();
        await productCollection.update({ id }, { $set: { ...productToUpdate } });
        res.json(productToUpdate);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.savedProducts = async (req, res, next) => {
    try {
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        const userSaved = await savedProductCollection.findOne({ userId: user.id });
        if (userSaved && userSaved.productIds) {
            const products = await productCollection.aggregate([
                { $sort: { createdAt: -1 } },
                { $match: { id: { $in: userSaved.productIds } } },
            ]);
            await products.map((product) => (product.saved = true));
            res.json(products);
        }
        else {
            res.json([]);
        }
        res.status(200);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.searchProducts = async (req, res, next) => {
    try {
        const params = req.query;
        if (Object.keys(params).length === 0) {
            throw new Error('Parameters are not specified');
        }
        const { keywords, location, priceFrom, priceTo, limit, offset } = params;
        let searchQuery = '';
        let sortByScore = '';
        if (keywords && keywords.length) {
            searchQuery = {
                $search: {
                    text: {
                        query: keywords.replace('+', ' ').toString(),
                        path: ['title', 'description'],
                    },
                },
            };
            sortByScore = {
                $sort: {
                    score: {
                        $meta: 'textScore',
                    },
                },
            };
        }
        const queryMatch = { $match: {} };
        if (priceTo) {
            queryMatch.$match.price = { $gte: Number(priceFrom) || 0, $lte: Number(priceTo) };
        }
        else {
            queryMatch.$match.price = { $gte: Number(priceFrom) || 0 };
        }
        if (location) {
            queryMatch.$match.location = location.replace('+', ' ');
        }
        const products = await productCollection.aggregate(searchQuery ? [
            searchQuery,
            queryMatch,
            {
                $skip: offset || 0,
            },
            {
                $limit: limit || 20,
            },
            sortByScore,
        ] : [queryMatch,
            {
                $skip: offset || 0,
            },
            {
                $limit: limit || 20,
            }]);
        res.json(products);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.userProducts = async (req, res, next) => {
    try {
        const { id } = req.params;
        if (!id) {
            throw new Error('id should be specified');
        }
        const products = await productCollection.aggregate([
            { $sort: { createdAt: -1 } },
            { $match: { ownerId: id } },
        ]);
        res.json({ list: products, count: products.length });
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.saveProduct = async (req, res, next) => {
    try {
        const { params } = req;
        const productId = params.id;
        if (!productId) {
            throw new Error('productId is not specified');
        }
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        const usersSaved = await savedProductCollection.findOne({ userId: user.id });
        const product = await productCollection.findOne({ id: productId });
        let updated;
        let created;
        if (!productId || !product) {
            throw new Error(`No product with such id ${productId}`);
        }
        if (usersSaved) {
            updated = await savedProductCollection.update({ userId: user.id }, { $addToSet: { productIds: productId } });
        }
        else {
            created = await savedProductCollection.insert({
                userId: user.id,
                productIds: [productId],
            });
        }
        res.json((updated && updated.nModified) || created ? 'Saved' : 'Already was saved');
        res.status(200);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.unsaveProduct = async (req, res, next) => {
    try {
        const { params } = req;
        const productId = params.id;
        if (!productId) {
            throw new Error('productId is not specified');
        }
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        const updated = await savedProductCollection.update({ userId: user.id }, { $pull: { productIds: productId } });
        res.json(updated && updated.nModified ? 'Unsaved' : 'Not in saved');
        res.status(200);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.removeProduct = async (req, res, next) => {
    try {
        const { id } = req.params;
        const product = await productCollection.findOne({ id });
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        if (!product) {
            throw new Error(`No product with such id ${id}`);
        }
        if (user.id !== product.ownerId) {
            throw new Error('User is not product owner');
        }
        const removed = await productCollection.remove({ id });
        res.json(removed);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
