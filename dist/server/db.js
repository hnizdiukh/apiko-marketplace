"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const monk_1 = __importDefault(require("monk"));
require('dotenv').config();
const DB = monk_1.default(process.env.MONGODB_URI || '');
exports.default = DB;
