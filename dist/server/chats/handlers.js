"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessages = exports.sendMessage = exports.getChats = exports.createChat = void 0;
const db_1 = __importDefault(require("../db"));
const getUserFromJWT_1 = __importDefault(require("../utils/getUserFromJWT"));
const validateAuthHeaders_1 = __importDefault(require("../utils/validateAuthHeaders"));
const db_2 = require("./db");
const productCollection = db_1.default.get('products');
const chatCollection = db_1.default.get('chats');
const messageCollection = db_1.default.get('messages');
const usersCollection = db_1.default.get('users');
exports.createChat = async (req, res, next) => {
    try {
        const { params } = req;
        const productId = params.id;
        if (!productId) {
            throw new Error('id is not specified');
        }
        const product = await productCollection.findOne({ id: productId });
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        if (!product) {
            throw new Error(`No product with such id ${productId}`);
        }
        const owner = await usersCollection.findOne({ id: product.ownerId });
        delete owner.password;
        if (!owner) {
            throw new Error('Owner of the product is not found');
        }
        if (user.id === owner.id) {
            throw new Error('Can not create chat with yourself');
        }
        const existingChat = await chatCollection.findOne({
            product,
            participants: [owner, user],
        });
        if (existingChat) {
            res.json(existingChat);
            return;
        }
        const chat = {
            id: `${product.id}-${user.id}`,
            productId,
            ownerId: product.ownerId,
            createdAt: Date.now(),
            updatedAt: Date.now(),
            product: { ...product },
            participants: [owner, user],
        };
        await chatCollection.insert({ ...chat });
        res.json(chat);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.getChats = async (req, res, next) => {
    try {
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        if (!user) {
            throw new Error('Not authorized');
        }
        const chats = await chatCollection.aggregate([
            { $match: { participants: { $elemMatch: { id: user.id } } } },
            { $sort: { updatedAt: -1 } },
        ]);
        res.json(chats);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.sendMessage = async (req, res, next) => {
    try {
        const { params } = req;
        const { body } = req;
        const chatId = params.id;
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        if (!chatId) {
            throw new Error('Invalid chat id');
        }
        const message = await db_2.insertMessage({ chatId, userId: user.id, text: body.text });
        await chatCollection.update({ id: chatId }, { $set: { lastMessage: message } });
        res.json(message);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.getMessages = async (req, res, next) => {
    try {
        const { params } = req;
        const limit = Number(params.limit) || 20;
        const from = Number(params.from) || 0;
        const chatId = params.id;
        if (!chatId) {
            throw new Error('Invalid id');
        }
        const chat = await chatCollection.findOne({ id: chatId });
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        if (chat ? !chat.participants.some((u) => u.id === user.id) : true) {
            throw new Error('User is not a participant for this chat');
        }
        const messages = await messageCollection.aggregate([
            { $match: { chatId, id: { $gte: from } } },
            { $sort: { createdAt: -1 } },
            { $limit: limit },
        ]);
        if (!messages) {
            throw new Error('No messages found');
        }
        res.json(messages);
        res.status(200);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
