"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getChatPartner = exports.insertMessage = void 0;
const db_1 = __importDefault(require("../db"));
const messageCollection = db_1.default.get('messages');
const chatCollection = db_1.default.get('chats');
exports.insertMessage = async (msg) => {
    const { chatId, userId, text } = msg;
    const messagesCount = await messageCollection.count({ chatId });
    const message = {
        id: messagesCount,
        chatId,
        ownerId: userId,
        text,
        read: false,
        createdAt: Date.now(),
        updatedAt: null
    };
    await messageCollection.insert(message);
    return message;
};
exports.getChatPartner = async (chatId, userId) => {
    const chat = await chatCollection.findOne({ id: chatId });
    const partner = chat ? chat.participants.filter((p) => p.id !== userId) : [];
    return partner[0];
};
