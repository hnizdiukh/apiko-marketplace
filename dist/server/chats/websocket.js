"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ws_1 = require("ws");
const getUserFromJWT_1 = __importDefault(require("../utils/getUserFromJWT"));
const db_1 = require("./db");
require('dotenv').config();
const webSockets = new Map();
const startWebSocket = (app) => {
    try {
        const wss = new ws_1.Server({ server: app });
        console.log('Starting websocket');
        wss.on('connection', (ws) => {
            ws.on('message', async (message) => {
                const parsed = JSON.parse(message);
                let user;
                let partner;
                let partnerWS;
                switch (parsed.type) {
                    case 'connection':
                        if (!parsed.data) {
                            break;
                        }
                        user = await getUserFromJWT_1.default(parsed.data.toString());
                        if (!user) {
                            break;
                        }
                        webSockets.set(user.id, ws);
                        break;
                    case 'message':
                        partner = await db_1.getChatPartner(parsed.data.chatId, parsed.data.ownerId);
                        partnerWS = webSockets.get(partner.id);
                        if (partnerWS && partnerWS.readyState === 1) {
                            partnerWS.send(JSON.stringify(parsed.data));
                        }
                        break;
                    default:
                        break;
                }
            });
            ws.on('close', (e) => {
                console.log('WS user disconected ', e);
            });
        });
    }
    catch (error) {
        console.error(error);
    }
};
exports.default = startWebSocket;
