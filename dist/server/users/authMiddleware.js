"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
require('dotenv').config();
const authMiddleware = async (req, res, next) => {
    try {
        const { headers: { authorization } } = req;
        if (!authorization) {
            res.status(401);
            throw new Error('Not authorized');
        }
        const token = authorization.split(' ')[1];
        if (!token) {
            res.status(401);
            throw new Error('Not authorized');
        }
        if ((await jsonwebtoken_1.default.verify(token, process.env.JWT_KEY || '3cab0ec2386b73e5ca92c4799f9441cbf58820dbedbf8a99'))) {
            await next();
        }
        else {
            res.status(401);
            throw new Error('Invalid JWT token');
        }
    }
    catch (error) {
        next(error);
    }
};
exports.default = authMiddleware;
