"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkReset = exports.reset = exports.forgot = exports.updateUser = exports.getUser = exports.getCurrentUser = exports.register = exports.login = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const slugify_1 = __importDefault(require("slugify"));
const schemas_1 = require("../schemas");
const db_1 = __importDefault(require("../db"));
const getUserFromJWT_1 = __importDefault(require("../utils/getUserFromJWT"));
const validateAuthHeaders_1 = __importDefault(require("../utils/validateAuthHeaders"));
const generateToken_1 = __importDefault(require("../utils/generateToken"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const uuid_1 = require("uuid");
require('dotenv').config();
const usersCollection = db_1.default.get('users');
const resetCollection = db_1.default.get('reset');
exports.login = async (req, res, next) => {
    try {
        const { body } = req;
        const user = body.email ? await usersCollection.findOne({ email: body.email.toLowerCase() }) : null;
        if (!user) {
            throw new Error('Wrong credentials');
        }
        const passwordMatch = await bcrypt_1.default.compareSync(body.password, user.password || '');
        if (!passwordMatch) {
            throw new Error('Wrong password');
        }
        const token = generateToken_1.default({
            iss: user.email,
        });
        if (!token) {
            throw new Error('JWT token is not created');
        }
        const { password, ...userResponse } = user;
        res.status(200);
        res.json({ token, user: userResponse });
    }
    catch (error) {
        res.status(401);
        next(error);
    }
};
exports.register = async (req, res, next) => {
    try {
        const { body } = req;
        const user = (await schemas_1.userSchema.validate(body));
        const existedUser = await usersCollection.findOne({ email: user.email });
        if (existedUser) {
            throw new Error('User with such email is already exist');
        }
        const slug = slugify_1.default(user.fullName, { lower: true, strict: true, locale: 'en' });
        user.id = `${slug}-${(Date.now() % 1000) + 1}`;
        user.createdAt = Date.now();
        user.password = await bcrypt_1.default.hashSync(user.password, 10);
        const token = generateToken_1.default({
            iss: user.email,
        });
        if (!token) {
            throw new Error('JWT token is not created');
        }
        user.location = body.location || null;
        user.avatar = body.avatar || null;
        user.phone = body.phone || null;
        user.updatedAt = body.updatedAt || null;
        user.email = user.email.toLowerCase();
        await usersCollection.insert(user);
        const { password, ...userResponse } = user;
        res.status(201);
        res.json({ token, user: userResponse });
    }
    catch (error) {
        res.status(401);
        next(error);
    }
};
exports.getCurrentUser = async (req, res, next) => {
    try {
        const token = await validateAuthHeaders_1.default(req);
        const user = await getUserFromJWT_1.default(token);
        if (!user) {
            throw new Error('Wrong JWT token');
        }
        const { password, ...userResponse } = user;
        res.status(200);
        res.json(userResponse);
    }
    catch (error) {
        res.status(401);
        next(error);
    }
};
exports.getUser = async (req, res, next) => {
    const { id } = req.params;
    try {
        const user = await usersCollection.findOne({ id });
        if (!user) {
            throw new Error('No user with such id');
        }
        const { password, ...userResponse } = user;
        res.json(userResponse);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.updateUser = async (req, res, next) => {
    try {
        const { body } = req;
        const fieldsToUpdate = await schemas_1.userUpdateSchema.validate(body, { stripUnknown: true });
        const token = await validateAuthHeaders_1.default(req);
        const currentUser = await getUserFromJWT_1.default(token);
        if (!currentUser) {
            throw new Error('No user with such id');
        }
        fieldsToUpdate.updatedAt = Date.now();
        const updatedUserState = await usersCollection.update({ id: currentUser.id }, { $set: { ...fieldsToUpdate } });
        if (!updatedUserState.nModified && updatedUserState.ok) {
            throw new Error('No fields to update');
        }
        if (!updatedUserState.nModified && !updatedUserState.ok) {
            throw new Error('Can not be updated');
        }
        const updatedUser = await usersCollection.findOne({ id: currentUser.id });
        if (!updatedUser) {
            throw new Error('Something wrong with user id');
        }
        const { password, ...response } = updatedUser;
        res.json(response);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.forgot = async (req, res, next) => {
    const { email } = req.body;
    try {
        if (!email) {
            throw new Error('Email is required');
        }
        const user = await usersCollection.findOne({ email: email.toLowerCase() });
        if (!user) {
            throw new Error('No user with such email');
        }
        const transporter = nodemailer_1.default.createTransport({ host: process.env.EMAIL_HOST,
            port: 465,
            secure: true,
            auth: {
                user: process.env.EMAIL_LOGIN,
                pass: process.env.EMAIL_PASSWORD
            } });
        const id = uuid_1.v4();
        const info = await transporter.sendMail({
            from: process.env.EMAIL_LOGIN,
            to: email,
            subject: "Marketplace Restore password",
            text: "Password reset link",
            html: `<p>Click on this link to restore your password for Marketplace website</p>
      ${req.protocol}://${req.headers.host}/#/reset/${id}/
      `,
        });
        await resetCollection.remove({ email });
        await resetCollection.insert({ id, email, createdAt: Date.now() });
        const { password, ...userResponse } = user;
        res.json(userResponse);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.reset = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { newPassword } = req.body;
        if (!newPassword) {
            throw new Error("newPassword is required");
        }
        const hashedPassword = await bcrypt_1.default.hashSync(newPassword, 10);
        const reset = await resetCollection.findOne({ id });
        if (!reset || !reset.email) {
            throw new Error("Link for reset is not valid");
        }
        await usersCollection.update({ email: reset.email }, { $set: { password: hashedPassword } });
        await resetCollection.remove({ email: reset.email });
        res.status(200);
        res.json('OK');
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.checkReset = async (req, res, next) => {
    try {
        const { id } = req.params;
        const reset = await resetCollection.findOne({ id });
        res.status(200);
        res.json(reset);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
