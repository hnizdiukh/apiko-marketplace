"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userUpdateSchema = exports.userSchema = exports.productSchema = void 0;
const yup = __importStar(require("yup"));
exports.productSchema = yup.object({
    ownerId: yup.string(),
    title: yup.string().trim().min(2).required(),
    description: yup.string(),
    photos: yup.array(yup.string().trim().url()),
    location: yup.string().trim().min(2).required(),
    price: yup.number().required(),
    createdAt: yup.number().nullable(),
    updatedAt: yup.number().nullable(),
    saved: yup.boolean(),
});
exports.userSchema = yup.object({
    email: yup.string().email().trim().min(4)
        .required(),
    password: yup.string().trim().min(4).required(),
    fullName: yup.string().trim().min(2).required(),
    createdAt: yup.number().nullable(),
    updatedAt: yup.number().nullable(),
    location: yup.string().trim().min(1),
    avatar: yup.string().trim().min(3).url(),
    phone: yup.string().nullable(),
});
exports.userUpdateSchema = yup.object({
    fullName: yup.string().trim().min(2),
    location: yup.string().trim().min(1),
    avatar: yup.string().trim().url().nullable(),
    phone: yup.string().trim().min(5).nullable(),
    updatedAt: yup.number().nullable(),
});
