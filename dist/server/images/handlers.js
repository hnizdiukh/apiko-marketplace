"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uploadImages = async (req, res, next) => {
    try {
        const { body } = req;
        res.status(200);
        res.json(body);
    }
    catch (error) {
        res.status(422);
        next(error);
    }
};
exports.default = uploadImages;
