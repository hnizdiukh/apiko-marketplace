"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validateAuthHeaders = (req) => {
    const { headers: { authorization } } = req;
    if (!authorization) {
        return;
    }
    const token = authorization.split(' ')[1];
    return token;
};
exports.default = validateAuthHeaders;
