"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
require('dotenv').config();
const key = process.env.JWT_KEY || '3cab0ec2386b73e5ca92c4799f9441cbf58820dbedbf8a99';
const generateToken = (payload) => jsonwebtoken_1.default.sign(payload, key, { algorithm: 'HS256' });
exports.default = generateToken;
