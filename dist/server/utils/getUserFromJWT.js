"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const db_1 = __importDefault(require("../db"));
require('dotenv').config();
const usersCollection = db_1.default.get('users');
const getUserFromJWT = async (token) => {
    try {
        if (!token) {
            return;
        }
        const decoded = await jsonwebtoken_1.default.verify(token, process.env.JWT_KEY || '');
        if (!decoded || !decoded.iss) {
            throw new Error('Invalid JWT');
        }
        const user = await usersCollection.findOne({ email: decoded.iss });
        if (!user) {
            return;
        }
        delete user.password;
        return user;
    }
    catch (error) {
        return;
    }
};
exports.default = getUserFromJWT;
