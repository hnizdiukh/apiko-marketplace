"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const handlers_1 = require("./server/products/handlers");
const handlers_2 = require("./server/users/handlers");
const handlers_3 = require("./server/chats/handlers");
const authMiddleware_1 = __importDefault(require("./server/users/authMiddleware"));
const websocket_1 = __importDefault(require("./server/chats/websocket"));
const handlers_4 = __importDefault(require("./server/images/handlers"));
const app = express_1.default();
const jsonParser = body_parser_1.default.json();
app.use(morgan_1.default('common'));
app.use(helmet_1.default({
    contentSecurityPolicy: false
}));
app.use(cors_1.default());
const port = Number(process.env.PORT) || 3333;
app.use(express_1.default.static('client/build'));
app.get('/api/v1', (_, res) => {
    res.send('Marketplace API');
});
app.post('/api/v1/auth/login', jsonParser, handlers_2.login);
app.post('/api/v1/auth/register', jsonParser, handlers_2.register);
app.post('/api/v1/auth/forgot', jsonParser, handlers_2.forgot);
app.get('/api/v1/auth/reset/:id', handlers_2.checkReset);
app.post('/api/v1/auth/reset/:id', jsonParser, handlers_2.reset);
app.get('/api/v1/account/user', jsonParser, authMiddleware_1.default, handlers_2.getCurrentUser);
app.get('/api/v1/users/:id', authMiddleware_1.default, handlers_2.getUser);
app.put('/api/v1/account/user', jsonParser, authMiddleware_1.default, handlers_2.updateUser);
app.get('/api/v1/products/latest', handlers_1.getProducts);
app.post('/api/v1/products', jsonParser, authMiddleware_1.default, handlers_1.postProduct);
app.get('/api/v1/products/saved', authMiddleware_1.default, handlers_1.savedProducts);
app.get('/api/v1/products/search', handlers_1.searchProducts);
app.get('/api/v1/products/:id', handlers_1.getSingleProduct);
app.put('/api/v1/products/:id', jsonParser, authMiddleware_1.default, handlers_1.updateProduct);
app.delete('/api/v1/products/:id', jsonParser, authMiddleware_1.default, handlers_1.removeProduct);
app.post('/api/v1/products/:id/save', authMiddleware_1.default, handlers_1.saveProduct);
app.post('/api/v1/products/:id/unsave', jsonParser, authMiddleware_1.default, handlers_1.unsaveProduct);
app.get('/api/v1/users/:id/products', handlers_1.userProducts);
app.post('/api/v1/products/:id/createChat', authMiddleware_1.default, handlers_3.createChat);
app.get('/api/v1/chats', authMiddleware_1.default, handlers_3.getChats);
app.post('/api/v1/chats/:id/messages', jsonParser, authMiddleware_1.default, handlers_3.sendMessage);
app.get('/api/v1/chats/:id/messages', authMiddleware_1.default, handlers_3.getMessages);
app.post('/api/v1/upload/images', authMiddleware_1.default, handlers_4.default);
app.get('/api/v1/ping', (_, res) => {
    res.status(200);
    res.json('pong');
});
app.use((req, res, next) => {
    const error = new Error(`Not Fount - ${req.url}`);
    res.status(404);
    next(error);
});
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
    const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
    res.status(statusCode);
    res.json({
        message: error.message
    });
});
const server = app.listen(port, () => console.log(`Listening on http://localhost:${port}`));
websocket_1.default(server);
