import express, { Application, Request, Response, NextFunction } from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';
import {
  getProducts,
  postProduct,
  savedProducts,
  searchProducts,
  getSingleProduct,
  updateProduct,
  saveProduct,
  unsaveProduct,
  userProducts,
  removeProduct
} from './server/products/handlers';
import {
  login,
  register,
  forgot,
  reset,
  getCurrentUser,
  getUser,
  updateUser,
  checkReset,
} from './server/users/handlers';
import {
  createChat,
  getChats,
  sendMessage,
  getMessages,
} from './server/chats/handlers';
import authMiddleware from './server/users/authMiddleware';
import startWebSocket from './server/chats/websocket';

const app: Application = express();

const jsonParser = bodyParser.json();

app.use(morgan('common'));
app.use(helmet({
  contentSecurityPolicy: false
}));
app.use(cors());

const port = Number(process.env.PORT) || 3333;

app.use(express.static('client/build'));

app.get('/api/v1', (_, res: Response) => {
  res.send('Marketplace API');
});

app.post('/api/v1/auth/login', jsonParser, login);
app.post('/api/v1/auth/register', jsonParser, register);

app.post('/api/v1/auth/forgot', jsonParser, forgot);
app.get('/api/v1/auth/reset/:id', checkReset);
app.post('/api/v1/auth/reset/:id', jsonParser, reset);

app.get('/api/v1/account/user', jsonParser, authMiddleware, getCurrentUser);
app.get('/api/v1/users/:id', authMiddleware, getUser);
app.put('/api/v1/account/user', jsonParser, authMiddleware, updateUser);

app.get('/api/v1/products/latest', getProducts);
app.post('/api/v1/products', jsonParser, authMiddleware, postProduct);
app.get('/api/v1/products/saved', authMiddleware, savedProducts);
app.get('/api/v1/products/search', searchProducts);
app.get('/api/v1/products/:id', getSingleProduct);
app.put('/api/v1/products/:id', jsonParser, authMiddleware, updateProduct);
app.delete('/api/v1/products/:id', jsonParser, authMiddleware, removeProduct);
app.post('/api/v1/products/:id/save', authMiddleware, saveProduct);
app.post('/api/v1/products/:id/unsave', jsonParser, authMiddleware, unsaveProduct);

app.get('/api/v1/users/:id/products', userProducts);

app.post('/api/v1/products/:id/createChat', authMiddleware, createChat);
app.get('/api/v1/chats', authMiddleware, getChats);
app.post('/api/v1/chats/:id/messages', jsonParser, authMiddleware, sendMessage);
app.get('/api/v1/chats/:id/messages', authMiddleware, getMessages);

app.get('/api/v1/ping', (_, res: Response) => {
  res.status(200);
  res.json('pong');
});

app.use((req: Request, res: Response, next: NextFunction) => {
  const error = new Error(`Not Fount - ${req.url}`);
  res.status(404);
  next(error);
});

// eslint-disable-next-line no-unused-vars
app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
  const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
  res.status(statusCode);
  res.json({
    message: error.message
  });
});

const server = app.listen(port, () => console.log(`Listening on http://localhost:${port}`));

startWebSocket(server);